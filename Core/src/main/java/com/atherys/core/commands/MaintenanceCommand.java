package com.atherys.core.commands;

import com.atherys.heroesaddon.util.Util;
import org.bukkit.command.CommandSender;

import java.util.Objects;

/**
 * @author apteryx
 * @time 4:15 PM
 * @since 1/18/2017
 */

public class MaintenanceCommand extends AtherysCommand {

    public MaintenanceCommand() {
        super("maintenancemode", 1, 1, false);
    }

    @Override
    public void run(CommandSender sender, String[] args) {
        if (sender.hasPermission("core.maintenance") || sender.isOp()) {
            if (Objects.equals(args[0], "on")) {
            } else if ( Objects.equals(args[0], "off") ) {
            }
            Util.sendMsg(sender,"There is nothing here yet, but there will be soon!");
        }
    }
}
