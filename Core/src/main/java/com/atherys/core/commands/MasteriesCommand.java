package com.atherys.core.commands;

import com.atherys.core.Core;
import com.atherys.heroesaddon.util.Util;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.classes.HeroClass;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.Set;

public class MasteriesCommand extends AtherysCommand {
    
    public MasteriesCommand() {
        super("masteries", 0, 1, true);
    }

    @Override
    public void run(CommandSender sender, String[] args) {
        Player player = (Player) sender;
        if (args.length == 1) {
            player = Bukkit.getPlayer(args[0]);
            if (player == null) {
                Util.sendMsg(sender, "Offline players are not supported.");
                return;
            }
        }
        Util.sendMsg(sender, printMasterClasses(player));
    }

    private String printMasterClasses(Player target) {
        Hero hero = Core.getHero(target);

        StringBuilder sb = new StringBuilder();
        sb.append(ChatColor.GRAY).append(target.getName()).append(" is master ");

        Set<HeroClass> classes = Heroes.getInstance().getClassManager().getClasses();

        int i = 0;

        for ( HeroClass hc : classes ) {
            i++;
            if ( !hero.isMaster(hc) ) {
                continue;
            }

            ChatColor color;

            if (isOrChildOf(hc, "Initiate")) {
                color = ChatColor.BLUE;
            } else if (isOrChildOf(hc, "Knave")) {
                color = ChatColor.DARK_GRAY;
            } else if (isOrChildOf(hc, "Squire")) {
                color = ChatColor.DARK_RED;
            } else {
                color = ChatColor.GREEN;
            }

            sb.append(color).append(hc.getName()).append(ChatColor.GRAY).append("; ");
        }

        return sb.toString();
    }

    private boolean isOrChildOf(HeroClass clazz, String check) {
        String name = clazz.getName();
        List<HeroClass> parents = clazz.getParents();
        return name.equalsIgnoreCase(check) || (parents.size() >= 1 && parents.get(0).getName().equalsIgnoreCase(check));
    }

}
