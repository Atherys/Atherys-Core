package com.atherys.core.commands.old;

import com.atherys.core.Core;
import com.atherys.core.commands.AtherysCommand;
import com.herocraftonline.heroes.characters.Hero;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class HeroAdminPartySetLeader extends AtherysCommand {

	public HeroAdminPartySetLeader(String label, int minArgs, int maxArgs) {
		super("HAPartySetLeader", 1, 2, true);
	}

	@Override
	public void run(CommandSender sender, String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage("This command cannot be executed from console");
			return;
		}
		Player psender = (Player) sender;
		if (args.length == 1){
			Hero heroLeader = Core.getHeroes().getCharacterManager().getHero(Bukkit.getPlayer(args[0]));
			Hero senderLeader = Core.getHeroes().getCharacterManager().getHero(psender);
			if (heroLeader.hasParty() && heroLeader.getParty().getLeader()== heroLeader){
				if (!heroLeader.getParty().getMembers().contains(senderLeader)){
					heroLeader.getParty().addMember(senderLeader);
				}
				heroLeader.getParty().setLeader(senderLeader);
				return;
			}else {
				psender.sendMessage("Sorry that player is not the party leader");
			}
		}
		if (args.length == 2){
			Hero heroLeader = Core.getHeroes().getCharacterManager().getHero(Bukkit.getPlayer(args[0]));
			Hero heroNewLeader = Core.getHeroes().getCharacterManager().getHero(Bukkit.getPlayer(args[1]));
			if (heroLeader.hasParty() && heroLeader.getParty().getLeader()== heroLeader){
				if (!heroLeader.getParty().getMembers().contains(heroNewLeader)){
					heroLeader.getParty().addMember(heroNewLeader);
				}
				heroLeader.getParty().setLeader(heroNewLeader);
				return;
			}else {
				psender.sendMessage("Sorry that player is not the party leader");
			}
		}
		psender.sendMessage("Something wrong happened!");
		return;


	}//end of run command
}//end of class
