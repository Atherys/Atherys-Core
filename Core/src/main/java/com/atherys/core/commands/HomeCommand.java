package com.atherys.core.commands;

import com.atherys.core.Core;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.characters.Hero;
import com.palmergames.bukkit.towny.Towny;
import com.palmergames.bukkit.towny.exceptions.NotRegisteredException;
import com.palmergames.bukkit.towny.exceptions.TownyException;
import com.palmergames.bukkit.towny.object.Resident;
import com.palmergames.bukkit.towny.object.Town;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

public class HomeCommand extends AtherysCommand {

    private static int millisecondsToWait = 10000;

    public HomeCommand() {
        super("home", 0, 0, true);
    }

    Player player;
    @Override
    public void run(final CommandSender sender, String[] args) {
        if ( sender instanceof Player ) {

            player = (Player) sender;

            Resident res = getResident(player);

            if ( res == null ) {
                player.sendMessage(ChatColor.RED + "Could not find Resident object. Please report this error.");
                return;
            }

            if ( res.hasTown() ) {
                try {
                    Town t = res.getTown();

                    if ( checkInCombat(player) ) return;

                    final Location spawn = t.getSpawn();

                    informSpawning(player);
                    scheduleSpawn(player, spawn);


                } catch (TownyException e) {
                    player.sendMessage(ChatColor.RED + "Towny Error. Please report.");
                    e.printStackTrace();
                }
            }
        }
    }

    private static boolean checkInCombat ( Player player ) {
        Hero hero = Heroes.getInstance().getCharacterManager().getHero(player);

        if ( hero.isInCombat() ) {
            player.sendMessage(ChatColor.RED + "You are currently in combat and cannot do that!");
            return true;
        }
        return false;
    }

    private static void informSpawning ( Player player ) {
        player.sendMessage(ChatColor.GREEN + "" + ChatColor.BOLD + "Teleporting you home in " + millisecondsToWait/1000 + " seconds.");

        for ( Entity e : player.getNearbyEntities( 15, 15, 15 ) ) {
            if ( e instanceof Player ) {
                e.sendMessage(ChatColor.GREEN + "" + ChatColor.BOLD + player.getName() + " will spawn home in " + millisecondsToWait/1000 + " seconds.");
            }
        }
    }

    private static void scheduleSpawn ( Player player, Location loc ) {
        Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(Core.getInstance(), () -> {
            if ( player.isOnline() && player.isValid() && !player.isDead() ) {
                player.teleport(loc);
            }
        }, millisecondsToWait/50 );
    }

    private static Resident getResident(Player player) {
        Resident resident;
        try {
            resident = Towny.plugin.getTownyUniverse().getResident(player.getName());
            return resident;
        } catch (NotRegisteredException e) {
            e.printStackTrace();
        }
        return null;
    }
}