/*
 * Copyright (c) 2016. A'therys Ascended
 *
 * This source code is proprietary software and must not be redistributed without A'therys Ascended's approval
 * 
 * File Created by willies952002
 */
package com.atherys.core.commands;

import com.atherys.heroesaddon.util.Util;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventException;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.plugin.RegisteredListener;

import java.util.logging.Level;

public class HeroDebugCommand extends AtherysCommand {

    public HeroDebugCommand() {
        super("herodebug", 0, 1, true);
    }

    @Override
    public void run(CommandSender sender, String[] args) {
        Player player = (Player) sender;
        if (!player.isOp()) {
            Util.sendMsg(player, "&cError: &6Only OPs Can Run This Command");
            return;
        }
        if (args.length < 1) {
            Util.sendMsg(player, "&cError: &6Please Specify a player");
            return;
        }
        Player target = Bukkit.getPlayer(args[0]);
        if (target == null) {
            Util.sendMsg(player, "&cError: &6No Such Player Online: &e" + args[0]);
            return;
        }
        EntityDamageEvent event = new EntityDamageEvent(target, EntityDamageEvent.DamageCause.ENTITY_ATTACK, 10.0);
        for (RegisteredListener listener : event.getHandlers().getRegisteredListeners()) {
            try {
                double damage = event.getDamage();
                listener.callEvent(event);
                if (event.isCancelled()) {
                    Util.sendMsg(player, "&cEntityDamage cancelled by: &7" + listener.getListener());
                }
                if (event.getDamage() != damage) {
                    Util.sendMsg(player, "&cEntityDamage damage changed by: &7" + listener.getListener());
                }
            } catch (EventException e) {
                Bukkit.getLogger().log(
                        Level.SEVERE,
                        "Error Caused By "
                                + listener.getListener()
                                + " from Hero Debug Command. ("
                                + e.getCause()
                                + ")"
                );
                e.printStackTrace();
            }
        }
        EntityDamageByEntityEvent event2 = new EntityDamageByEntityEvent(player, target, EntityDamageEvent.DamageCause.ENTITY_ATTACK, 10.0);
        for (RegisteredListener listener : event2.getHandlers().getRegisteredListeners()) {
            try {
                double damage = event2.getDamage();
                listener.callEvent(event2);
                if (event2.isCancelled()) {
                    Util.sendMsg(player, "&cEntityDamageByEntity cancelled by: &7" + listener.getListener());
                }
                if (event2.getDamage() != damage) {
                    Util.sendMsg(player, "&cEntityDamageByEntity damage changed by: &7" + listener.getListener());
                }
            } catch (EventException e) {
                Bukkit.getLogger().log(
                        Level.SEVERE,
                        "Error Caused By "
                                + listener.getListener()
                                + " from Hero Debug Command. ("
                                + e.getCause()
                                + ")"
                );
                e.printStackTrace();
            }
        }
    }

}
