package com.atherys.core.commands;

import com.atherys.core.utils.EffectEnchantmentUtil;
import org.apache.commons.lang3.EnumUtils;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class SpecialFXCommand extends AtherysCommand {

    private enum ParamPart {
        DECLARATION(0),
        BODY(1);

        private final int value;

        ParamPart( int value ) { this.value = value; }
    }

    public SpecialFXCommand() {
        super("specialfx", 2, 5, true);
    }

    @Override
    public void run(CommandSender sender, String[] args) {
        Player player = (Player) sender;

        ItemStack item = player.getInventory().getItemInMainHand();
        if ( item == null || item.getType().equals(Material.AIR) ) {
            player.sendMessage(ChatColor.RED + "You must hold an item first in order to give it a special effect!");
            return;
        }

        EffectEnchantmentUtil.EffectEnchantment ench = createEffectEnchantmentFromParams(player, args);
        if ( ench == null ) return;
        player.sendMessage( ench.toJson().toString() );
        player.sendMessage( ChatColor.GOLD + "Created enchantment. Applying..." );
        player.getInventory().setItemInMainHand( ench.applyTo(item) );
    }

    private EffectEnchantmentUtil.EffectEnchantment createEffectEnchantmentFromParams ( Player player, String[] params ) {
        Particle particle = null;
        Sound sound = null;

        String event = null;
        int count = -1;
        float chance = -1.0f;

        for ( String param : params ) {
            if ( !param.contains(":") ) {
                informPlayerInvalidParams(player);
                return null;
            } else {
                String[] par = param.split(":");
                String declaration = par[ParamPart.DECLARATION.value];
                String body = par[ParamPart.BODY.value];
                switch ( declaration ) {
                    case "p":
                        if ( EnumUtils.isValidEnum(Particle.class, body ) ) {
                            particle = Particle.valueOf(par[ParamPart.BODY.value]);
                        } else {
                            player.sendMessage(ChatColor.RED + "The particle you provided was invalid.");
                            return null;
                        }
                        break;
                    case "pc":
                        try {
                            count = Integer.valueOf(body);
                        } catch (NumberFormatException e) {
                            player.sendMessage(ChatColor.RED + "The particle count you provided was invalid.");
                            return null;
                        }
                        break;
                    case "s":
                        if ( EnumUtils.isValidEnum(Sound.class, body ) ) {
                            sound = Sound.valueOf(par[ParamPart.BODY.value]);
                        } else {
                            player.sendMessage(ChatColor.RED + "The sound you provided was invalid.");
                            return null;
                        }
                        break;
                    case "e":
                        if (EffectEnchantmentUtil.events.contains(body)) {
                            event = body;
                        } else {
                            player.sendMessage(ChatColor.RED + "The event you provided was invalid.");
                            return null;
                        }
                        break;
                    case "c":
                        try {
                            chance = Float.valueOf(body);
                        } catch (NumberFormatException e) {
                            player.sendMessage(ChatColor.RED + "The chance you provided was invalid.");
                            return null;
                        }
                        break;
                }
            }
        }

        if ( event == null || chance == -1.0f ) {
            informPlayerInvalidParams(player);
            return null;
        }

        EffectEnchantmentUtil.EffectEnchantment ench = new EffectEnchantmentUtil.EffectEnchantment( event, chance );

        if ( ( particle == null || count == -1 ) && sound == null ) {
            player.sendMessage(ChatColor.RED + "Cannot create enchantment with neither a particle nor a sound!");
            return null;
        }

        if ( particle != null && count != -1 ) {
            ench.setParticle(particle);
            ench.setParticleCount(count);
        } else {
            player.sendMessage(ChatColor.GREEN + "An enchantment with no particles will be created.");
        }

        if ( sound != null ) {
            ench.setSound(sound);
        } else {
            player.sendMessage(ChatColor.GREEN + "An enchantment with no sounds will be created.");
        }

        return ench;
    }
    
    private void informPlayerInvalidParams ( Player player ) {
        player.sendMessage(ChatColor.RED + "You must provide at least the event and chance out of the following parameters:");
        player.sendMessage(ChatColor.GOLD + "p:<particle>");
        player.sendMessage(ChatColor.GOLD + "pc:<particleCount>");
        player.sendMessage(ChatColor.GOLD + "s:<sound>");
        player.sendMessage(ChatColor.GOLD + "e:<event>");
        player.sendMessage(ChatColor.GOLD + "Currently available events: \n" + ChatColor.YELLOW + EffectEnchantmentUtil.events.toString() );
        player.sendMessage(ChatColor.GOLD + "c:<chance>");
    }
}
