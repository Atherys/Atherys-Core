/*
 * Copyright (c) 2016. A'therys Ascended
 *
 * This source code is proprietary software and must not be redistributed without A'therys Ascended's approval
 * 
 * File Created by willies952002
 */
package com.atherys.core.commands;

import com.atherys.core.Core;
import com.atherys.heroesaddon.util.Util;
import com.herocraftonline.heroes.characters.Hero;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Map;

public class BindsCommand extends AtherysCommand {

    public BindsCommand() {
        super("binds", 0, 0, true);
    }

    @Override
    public void run(CommandSender sender, String[] args) {
        Player player = (Player)sender;
        Hero hero = Core.getHero(player);
        Map<Material, String[]> bounded = hero.getBinds();
        if (bounded.size() > 0) {
            Util.sendMsg(player, "Current Binds:");
            for (Map.Entry binding : bounded.entrySet()) {
                Material tool = (Material) binding.getKey();
                String[] skill = (String[]) binding.getValue();
                Util.sendFMsg(player, " - %s: %s", tool.toString(), skill[0]);
            }
        } else {
            Util.sendMsg(player, "You have no bound skills");
        }
    }
}
