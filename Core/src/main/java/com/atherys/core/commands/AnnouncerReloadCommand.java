package com.atherys.core.commands;

import com.atherys.core.Core;
import com.atherys.core.announcer.Announcer;
import org.bukkit.command.CommandSender;

public class AnnouncerReloadCommand extends AtherysCommand {

    public AnnouncerReloadCommand() {
        super("announcerreload", 0, 0, true);
    }

    @Override
    public void run(CommandSender sender, String[] args) {
        Core.getInstance().reloadConfig();
        Announcer.getInstance().reload();
    }
}
