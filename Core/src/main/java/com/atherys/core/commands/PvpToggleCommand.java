package com.atherys.core.commands;

import com.atherys.core.Core;
import com.palmergames.bukkit.towny.Towny;
import com.palmergames.bukkit.towny.exceptions.NotRegisteredException;
import com.palmergames.bukkit.towny.object.Resident;
import com.palmergames.bukkit.towny.object.Town;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class PvpToggleCommand extends AtherysCommand {

    private static long timeBetweenSwitches = Core.getInstance().getSettings().getPvpToggleCooldown();
    private List<String> ranks = Arrays.asList("co-mayor");


    public PvpToggleCommand() {
        super("pvptoggle", 0, 0, true);
        createPvpToggleTable();
    }

    @Override
    public void run(CommandSender sender, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            Resident res = getResident(player);
            if (res == null) {
                player.sendMessage(ChatColor.RED + "Could not find Resident object. Please report this error.");
                return;
            }

            if (res.hasTown()) {
                try {
                    Town t = res.getTown();
                    if ( !canResidentTogglePvp(res) ) {
                        player.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "You must have a co-mayor rank ( or above ) in order to toggle Pvp.");
                        return;
                    }
                    ResultSet pvpToggleQuery = Core.getDb().query("SELECT * FROM core_pvpToggle WHERE TownUID='" + t.getUID() + "';");
                    Date time = new Date();
                    long unixTime = time.getTime() / 1000;

                    while (pvpToggleQuery.next()) {
                        int timestamp = pvpToggleQuery.getInt("timestamp");
                        long timeDif = unixTime - timestamp;
                        if (timeDif >= timeBetweenSwitches) {
                            // can switch
                            t.setPVP(!t.isPVP());
                            player.sendMessage(ChatColor.GREEN + "" + ChatColor.BOLD + "Town Pvp Toggled. Can toggle again in 12 hours.");
                            return;
                        } else {
                            // can't switch
                            int remainingTime = (int) (timeBetweenSwitches - timeDif);
                            int hours = (remainingTime % 86400 ) / 3600;
                            int mins = ((remainingTime % 86400 ) % 3600 ) / 60;
                            int secs = ((remainingTime % 86400 ) % 3600 ) % 60;
                            player.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "Town Pvp could not be toggled. Try again in " + hours + "h " + mins + "m " + secs + "s.");
                            return;
                        }
                    }

                    // if the above has not returned out of the method, that means the query was empty, and the town is not in the table
                    // so, add the town to the table.

                    Core.getDb().query("INSERT INTO core_pvpToggle ( TownUID, timestamp ) VALUES ('" + t.getUID() + "', '" + unixTime + "' );");
                    t.setPVP(!t.isPVP());
                    player.sendMessage(ChatColor.GREEN + "" + ChatColor.BOLD + "Town Pvp Toggled. Can toggle again in 12 hours.");

                } catch (NotRegisteredException e) {
                    player.sendMessage(ChatColor.RED + "Town is not registered. Please report this error.");
                    return;
                } catch (SQLException e) {
                    player.sendMessage(ChatColor.RED + "Something went wrong with the database. Please report this error: " + e.getErrorCode());
                    e.printStackTrace();
                    e.getErrorCode();
                }
            }
        }
        return;
    }

    private void createPvpToggleTable() {
        Core.getDb().query("CREATE TABLE IF NOT EXISTS `core_pvpToggle` ( `TownUID` INTEGER NOT NULL, `timestamp` INTEGER NOT NULL );");
    }

    private Resident getResident(Player player) {
        Resident resident;
        try {
            resident = Towny.plugin.getTownyUniverse().getResident(player.getName());
            return resident;
        } catch (NotRegisteredException e) {
            e.printStackTrace();
        }
        return null;
    }

    private boolean canResidentTogglePvp ( Resident res ) {
        if ( res.isMayor() ) return true;
        for ( String rank : res.getTownRanks() ) {
            if ( ranks.contains(rank) ) return true;
        }
        return false;
    }

}


