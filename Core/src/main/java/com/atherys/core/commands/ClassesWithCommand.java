/*
 * Copyright (c) 2016. A'therys Ascended
 *
 * This source code is proprietary software and must not be redistributed without A'therys Ascended's approval
 * 
 * File Created by willies952002
 */
package com.atherys.core.commands;

import com.atherys.core.Core;
import com.atherys.heroesaddon.util.Lib;
import com.atherys.heroesaddon.util.Util;
import com.herocraftonline.heroes.characters.classes.HeroClass;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;
import java.util.List;

public class ClassesWithCommand extends AtherysCommand {

    public ClassesWithCommand() {
        super("classeswith", 1, 1, true);
    }

    @Override
    public void run(CommandSender sender, String[] args) {
        boolean found = false;
        String skill = args[0];
        List<String> result = new ArrayList<>();
        result.add("&7Classes with the skill &f" + skill + "&7:");
        for (HeroClass heroClass : Core.getHeroes().getClassManager().getClasses()) {
            if (heroClass.hasSkill(skill)) {
                if (!heroClass.getName().equals("Admin") && !heroClass.getName().equals("Test")) {
                    result.add(" &7- " + Lib.getClassColor(heroClass) + heroClass.getName());
                    found = true;
                }
            }
        }
        if (found) {
            result.forEach(line -> Util.sendMsg(sender, line));
        } else {
            Util.sendMsg(sender, "&7No classes found with the skill &f" + skill + "&7.");
        }
    }

}
