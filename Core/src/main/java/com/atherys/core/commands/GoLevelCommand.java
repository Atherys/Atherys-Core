package com.atherys.core.commands;

import com.atherys.core.Core;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.characters.Hero;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

public class GoLevelCommand extends AtherysCommand {


    private static int millisecondsToWait = 10000;

    public GoLevelCommand() {
        super("golevel", 0, 0, true);
    }


    @Override
    public void run(final CommandSender sender, String[] args) {
        if (sender instanceof Player) {

            Player player = (Player) sender;

            if ( checkInCombat(player) ) return;

            informGoLevel(player);

            Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(Core.getInstance(), new Runnable() {
                @Override
                public void run() {
                    Core.connectPlayerToServer(player, Core.getInstance().getSettings().getPVEServerName());
                }
            }, millisecondsToWait / 50);

        }
    }

    private static boolean checkInCombat ( Player player ) {
        Hero hero = Heroes.getInstance().getCharacterManager().getHero(player);

        if ( hero.isInCombat() ) {
            player.sendMessage(ChatColor.RED + "You are currently in combat and cannot do this command.");
            return true;
        }
        return false;
    }

    private static void informGoLevel ( Player player ) {
        player.sendMessage(ChatColor.GREEN + "" + ChatColor.BOLD + "Teleporting you to goLevel in " + millisecondsToWait / 1000 + " seconds.");

        for (Entity e : player.getNearbyEntities(15, 15, 15)) {
            if (e instanceof Player) {
                e.sendMessage(ChatColor.GREEN + "" + ChatColor.BOLD + player.getName() + " will spawn goLevel in " + millisecondsToWait / 1000 + " seconds.");
            }
        }
    }
}
