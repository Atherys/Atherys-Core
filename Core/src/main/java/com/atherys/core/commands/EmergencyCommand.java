/*
 * Copyright (c) 2016. A'therys Ascended
 *
 * This source code is proprietary software and must not be redistributed without A'therys Ascended's approval
 * 
 * File Created by willies952002
 */
package com.atherys.core.commands;

import com.atherys.core.Core;
import com.atherys.heroesaddon.util.Confirmation;
import com.atherys.heroesaddon.util.Util;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;

public class EmergencyCommand extends AtherysCommand {

    public EmergencyCommand() {
        super("911", 0, 0, true);
    }

    @Override
    public void run(CommandSender sender, String[] args) {
        Player player = (Player) sender;
        if (!hasActivationPerms(player)) return;
        Confirmation.textConfirm(
                player,
                "Activate the Emergency Protocol?",
                "Launch The Nuke",
                "Abort Launch",
                p -> {
                    final int[] seconds = {5};
                    Util.log("[9/11] THE 911 PROTOCOL HAS BEEN ACTIVATED BY " + player.getName());
                    Bukkit.getScheduler().runTaskTimer(Core.getInstance(), () -> {
                        Util.forEachPlayer(pl -> Util.title(pl,
                                "&4Server Shutting Down",
                                "&4" + seconds[0] + " Seconds"
                        ));
                        if (seconds[0] == 0) {
                            Bukkit.getServer().shutdown();
                        }
                        seconds[0]--;
                    }, 20, 20);
                },
                HumanEntity::closeInventory
        );
    }

    private boolean hasActivationPerms(Player player) {
        boolean perm1 = player.hasPermission("atherys.emergency");
        boolean perm2 = player.hasPermission("heroesaddon.911");
        return perm1 || perm2 || player.isOp();
    }

}
