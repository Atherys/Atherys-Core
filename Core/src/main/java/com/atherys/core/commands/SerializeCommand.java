package com.atherys.core.commands;

import com.atherys.core.utils.SerializationUtil;
import com.google.gson.JsonObject;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class SerializeCommand extends AtherysCommand {

    public SerializeCommand() {
        super("serialize", 0, 0, true);
    }

    @Override
    public void run(CommandSender sender, String[] args) {
        Player player = (Player) sender;

        ItemStack itemInHand = player.getInventory().getItemInMainHand();
        if ( itemInHand == null || itemInHand.getType().equals(Material.AIR) ) {
            player.sendMessage(ChatColor.RED + "You must hold an item first in order to serialize it!");
            return;
        }

        JsonObject itemJson = SerializationUtil.itemToJson(itemInHand);
        player.sendMessage("Serialized Item:\n" + itemJson.toString().replace(ChatColor.COLOR_CHAR, '&') );
    }
}
