package com.atherys.core.commands;

import com.atherys.core.Core;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;
import java.util.List;

public class MobPackage1Command extends AtherysCommand {
    public MobPackage1Command() {
        super("MMPackage1",0,0, true);
    }

    @Override
    public void run(CommandSender sender, String[] args) {
        if (sender.isOp()) {
            List<String> commandlist = new ArrayList<>();
            commandlist.add("mythicmobs egg get WolfSpiderEvent 128");
            commandlist.add("mythicmobs egg get PlaguedSoulEvent 128");
            commandlist.add("mythicmobs egg get CalciniteEvent 128");
            for (String s2 : commandlist) {
                Core.getInstance().getServer().dispatchCommand(sender, s2);
            }
        }
    }
}
