/*
 * Copyright (c) 2016. A'therys Ascended
 *
 * This source code is proprietary software and must not be redistributed without A'therys Ascended's approval
 *
 * File Created by willies952002
 */
package com.atherys.core.commands;

import com.atherys.core.Core;
import com.atherys.heroesaddon.util.Lib;
import com.atherys.heroesaddon.util.Util;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.Effect;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class HeroEffectsCommand extends AtherysCommand {
    
    public HeroEffectsCommand() {
        super("heroeffects", 0, 1, false);
    }

    @Override
    public void run(CommandSender sender, String[] args) {
        if (!sender.isOp()) {
            Util.sendMsg(sender, "&4You don't have permission");
            return;
        }
        Hero hero = null;
        if (args.length == 0) {
            if (!(sender instanceof Player)) {
                Util.sendMsg(sender, "Specify a player to show their effects.");
                return;
            }
            hero = Core.getHero((Player) sender);
        }
        if (args.length == 1) {
            Player player = Bukkit.getPlayer(args[0]);
            if (player == null) {
                Util.sendMsg(sender, "Offline players are not supported.");
                return;
            }
            hero = Core.getHero(player);
        }
        if (hero != null) {
            Util.sendMsg(sender, "&7Current effects of &f" + hero.getName() + "&7:");
            for (Effect effect : hero.getEffects()) {
                String effName = effect.getName();
                String effClass = Lib.getEffectClass(effect);
                Util.sendMsg(sender, "&7- Name: &f" + effName + "&7, Type: &f" + effClass);
            }
        }
    }
    
}
