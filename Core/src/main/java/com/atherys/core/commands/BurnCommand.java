package com.atherys.core.commands;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.HashSet;

public class BurnCommand extends AtherysCommand {
    public BurnCommand() {
        super("burns",0,0,true);
    }

    @Override
    public void run(CommandSender sender, String[] args) {
        HashSet<Material> transparent = new HashSet<>();
        transparent.add(Material.AIR);
        /*

        ItemStack burnstick = new ItemStack(Material.STICK);
        ItemMeta meta = burnstick.getItemMeta();
        meta.setDisplayName("§3Stick of Fire");
        meta.setLore(Arrays.asList("§3Staff item only", "§3If you are found with it you will be punished"));
        burnstick.setItemMeta(meta);
        sender.getServer().getPlayer(sender.getName()).getInventory().addItem(burnstick);
        */
        Player player = (Player) sender;
        Block block =player.getTargetBlock(transparent,10).getLocation().add(0,1,0).getBlock();
        if (block.getType().equals(Material.AIR)){
            block.setType(Material.FIRE);
        }
        else {
            player.sendMessage("Sorry That block isnt a Air block!");
        }
    }
}
