package com.atherys.core.commands;

import com.atherys.core.Core;
import com.atherys.heroesaddon.util.Util;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

public abstract class AtherysCommand implements Listener {

    private String label;
    private int minArgs;
	private int maxArgs;
	private boolean playerOnly;

    private boolean disabled = false;

	/**
	 * @param label - The label of the command.
	 * @param minArgs - The minimum amount of arguments.
	 * @param maxArgs - The maximum amount of arguments.
     * @param playerOnly - Flag: Only run for players.
	 */
	public AtherysCommand(String label, int minArgs, int maxArgs, boolean playerOnly) {
        this.label = label;
        this.minArgs = minArgs;
		this.maxArgs = maxArgs;
		this.playerOnly = playerOnly;

        Bukkit.getLogger().info("[AtherysCore] Loading command: /" + label);

		register(label);
		Core.getCommands().put(label, this);
        Bukkit.getPluginManager().registerEvents(this, Core.getInstance());
	}

    private void register(String name) {
        Core.getInstance().getCommand(name).setExecutor(new CommandExecutor() {
            @Override
            public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
                if(Core.getCommands().containsKey(label)) {
                    AtherysCommand mod = Core.getCommands().get(label);
                    if (mod.playerOnly && !(commandSender instanceof Player)) {
                        Util.sendMsg(commandSender, "&cError: &6Only In-Game Players Can Use This Command");
                        return true;
                    }
                    if(Util.permCheck(commandSender, "atherys.command." + label)) {
                        if(args.length >= mod.minArgs && args.length <= mod.maxArgs) {
                            mod.run(commandSender, args);
                        } else {
                            Util.sendMsg(commandSender, "&cIncorrect Usage: &e" + command.getUsage());
                        }
                    } else {
                        Util.sendMsg(commandSender, "&cPermission Denied: &6Missing Required Permission");
                        Util.sendMsg(commandSender, "&cIf you believe this is in error, please contact the &4Administration.");
                    }
                }
                return false;
            }
        });
    }

    protected void disable() {
        this.disabled = true;
    }

    protected void enable() {
        this.disabled = false;
    }

	public abstract void run(CommandSender sender, String[] args);

    @EventHandler
    public void cancelCommand(PlayerCommandPreprocessEvent event) {
        if(event.getMessage().toLowerCase().startsWith("/" + label.toLowerCase()) && disabled) {
            if(!(event.isCancelled())) event.setCancelled(true);
        }
    }

}