package com.atherys.core;

import ca.xshade.bukkit.questioner.Questioner;
import com.atherys.core.announcer.Announcer;
import com.atherys.core.commands.*;
import com.atherys.core.events.*;
import com.atherys.core.settings.Settings;
import com.atherys.core.utils.DB;
import com.atherys.core.utils.EffectEnchantmentUtil;
import com.atherys.heroesaddon.events.HeroesListener;
import com.atherys.heroesaddon.events.PetListener;
import com.atherys.heroesaddon.events.TabCompleteListener;
import com.atherys.heroesaddon.util.Confirmation;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.characters.Hero;
import com.palmergames.bukkit.towny.Towny;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import ru.tehkode.permissions.bukkit.PermissionsEx;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author apteryx, haedhutner
 * @time 9:03 AM
 * @since 12/29/2016
 */

public class Core extends JavaPlugin {

    private static Core instance;
    private Settings settings;

    private static String[] dependencies = {
        "Heroes", "Towny", "PermissionsEx", "Questioner", "WorldGuard"
    };

    private static HashMap<String, AtherysCommand> commands;

    private static DB coreDatabase;

    private static Announcer announcer;

    @Override
    public void onEnable() {

        for ( String plugin : dependencies ) {
            if (Bukkit.getPluginManager().getPlugin(plugin) == null || !Bukkit.getPluginManager().getPlugin(plugin).isEnabled()) {
                Bukkit.getLogger().severe(plugin + " is required for core features. Shutting down...");
                Bukkit.getPluginManager().disablePlugin(this);
            } else {
                Bukkit.getLogger().info("[AtherysCore] Found " + plugin);
            }
        }

        instance = this;
        settings = new Settings();
        commands = new HashMap<>();
        announcer = Announcer.getInstance();
        EffectEnchantmentUtil.initialize(this);

        initDB();
        createData();
        initializeEvents();
        initializeCommands();

        getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
    }

    @Override
    public void onDisable() {
        instance = null;
        commands.clear();
        Bukkit.getLogger().info("[AtherysCore] Shutting down...");
    }

    private void createData() {
        if (!this.getDataFolder().exists()) {
            this.getDataFolder().mkdir();
            Bukkit.getLogger().info("mkdir /" + this.getName() +"/");
        }

        saveDefaultConfig();
    }

    public static Core getInstance() {
        return instance;
    }

    public Settings getSettings() {
        return settings;
    }

    private void initializeEvents() {
        PluginManager pm = Bukkit.getPluginManager();

        pm.registerEvents(new PlayerTeleportEvents(), this);
        pm.registerEvents(new BlockRelatedEvents(), this);
        pm.registerEvents(new DamageRelatedEvents(), this);
        pm.registerEvents(new PlayerInteractEvents(), this);
        pm.registerEvents(new PlayerJoinEvents(), this);
        pm.registerEvents(new PlayerPreLoginEvents(), this);
        pm.registerEvents(new BowShootEvents(), this);
        pm.registerEvents(new PetListener(), this);
        pm.registerEvents(new HeroesListener(), this);
        pm.registerEvents(new MobSpawnListener(), this);

        //pm.registerEvents(new HeroPartyEvents(), this);

        if (settings.getTabBlockCompleteEnabled()) {
            pm.registerEvents(new TabCompleteListener(), this);
        }
    }

    private void initializeCommands() {
        new BindsCommand();
        new HeroDebugCommand();
        new ClassesWithCommand();
        new HeroEffectsCommand();
        new EmergencyCommand();
        new MasteriesCommand();

        //new HeroAdminPartylistCMD();
        //new CooldownResetCMD();
        //new CooldownResetSubCommand();

        new MobPackage1Command();
        //getLogger().info("MobPackage1CMD loaded");
        new MobPackage2Command();
        //getLogger().info("MobPackage2CMD loaded");
        new MobPackage3Command();
        //getLogger().info("MobPackage3CMD loaded");
        new BurnCommand();
        //getLogger().info("burnCMD loaded");
        //new GoLevelCommand();
        //getLogger().info("GolevelGMD loaded");
        //new HomeCMD();
        //   home:
        //description: Go back to your town
        //permission: Atherys.Home

        //getLogger().info("HomeCMD loaded");
        new PvpToggleCommand();
        //getLogger().info("PvpCMD loaded");
        //Feel free to add more :)

        //new MaintenanceCommand();

        new SerializeCommand();

        new SpecialFXCommand();

        new HomeCommand();

        new Confirmation.ConfirmationCommand(); // Do not remove this. Is required for confirmation dialogue.
    }

    private void initDB() {
        Map<String,String> dbInfo = settings.getDatabaseInfo();

        for ( Map.Entry<String,String> setting : dbInfo.entrySet() ) {
            if ( setting.getValue() == null ) {
                Bukkit.getLogger().severe("[AtherysCore] Database information '" + setting.getKey() + "' missing. Aborting...");
                Bukkit.getPluginManager().disablePlugin(this);
                return;
            }
        }

        if ( dbInfo.get("sql").equals("mysql") ) {
            // if set to mysql, create mysql connection
            coreDatabase = new DB(dbInfo.get("ip"), Integer.valueOf(dbInfo.get("port")), dbInfo.get("db"), dbInfo.get("username"), dbInfo.get("password") );
            Bukkit.getLogger().info("[AtherysCore] MySQL Storage Initialized...");
        } else {
            // if set to anything else, create sqlite local database
            try {
                new File(this.getDataFolder(),"core.db").createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            coreDatabase = new DB( new File(this.getDataFolder(),"core.db").getPath() );
            Bukkit.getLogger().info("[AtherysCore] SQLite Storage Initialized...");
        }
    }

    public static Towny getTowny() {
        return Towny.plugin;
    }

    public static Heroes getHeroes() {
        return Heroes.getInstance();
    }

    public static PermissionsEx getPex() {
        return (PermissionsEx) Bukkit.getServer().getPluginManager().getPlugin("PermissionsEx");
    }

    public static WorldGuardPlugin getWorldGuard() {
        return WorldGuardPlugin.inst();
    }

    public static Questioner getQuestioner(){
        return (Questioner) Bukkit.getServer().getPluginManager().getPlugin("Questioner");
    }

    public static HashMap<String, AtherysCommand> getCommands() {
        return commands;
    }

    public static DB getDb() {
        return coreDatabase;
    }

    public static Hero getHero(Player player) {
        return getHeroes().getCharacterManager().getHero(player);
    }

    public static Announcer getAnnouncer() {
        return announcer;
    }

    public static void connectPlayerToServer ( Player player, String serverName ) {
        if (player.isOnline() && player.isValid() && !player.isDead()) {

            ByteArrayDataOutput out = ByteStreams.newDataOutput();
            out.writeUTF("Connect");
            out.writeUTF( serverName );

            player.sendPluginMessage(Core.getInstance(), "BungeeCord", out.toByteArray());

            /*
            String CMDstring = "warp golevel %staticPlayerObject%";
            CMDstring = CMDstring.replace("%staticPlayerObject%", player.getName());
            Core.getInstance().getServer().dispatchCommand(Core.getInstance().getServer().getConsoleSender(), CMDstring);
            */
        }
    }

    public static boolean checkInCombat ( Player player ) {
        Hero hero = Heroes.getInstance().getCharacterManager().getHero(player);

        if ( hero.isInCombat() ) {
            player.sendMessage(ChatColor.RED + "You are currently in combat and cannot do that!");
            return true;
        }
        return false;
    }


}
