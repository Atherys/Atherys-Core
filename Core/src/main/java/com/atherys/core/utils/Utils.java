package com.atherys.core.utils;

import com.atherys.core.Core;
import com.herocraftonline.heroes.characters.Hero;
import com.palmergames.bukkit.towny.exceptions.NotRegisteredException;
import com.palmergames.bukkit.towny.object.Coord;
import com.palmergames.bukkit.towny.object.TownyUniverse;

import java.util.ArrayList;
import java.util.List;

/**
 * @author apteryx
 * @time 12:53 AM
 * @since 1/14/2017
 */
public class Utils {

    public static int LARGE_RANGE = 50;
    public static int SHORT_RANGE = 20;

    /**
     * Takes a hero as a parameter and evaluates if the block the target is inside of a town
     * that has PvP enabled. Returns true by default in the event that the Towny plugin is not available.
     * @param hero The target to validate.
     * @return Assertion of the hero being a valid target or not.
     */

    public static boolean validTarget(Hero hero) {
        if (Core.getTowny() != null) {
            try {
                boolean isTown = TownyUniverse.getDataSource().getWorld(hero.getPlayer().getWorld().getName()).hasTownBlock(Coord.parseCoord(hero.getPlayer().getLocation()));
                boolean townPVP = TownyUniverse.getDataSource().getWorld(hero.getPlayer().getWorld().getName()).getTownBlock(Coord.parseCoord(hero.getPlayer().getLocation())).getTown().isPVP();
                if (isTown) {
                    if (townPVP) {
                        return true;
                    }
                    return false;
                }
            } catch (NotRegisteredException e) {
                e.printStackTrace();
            }
        }
        return true;
    }

    /**
     * Gets the locations mathematically for the particles around the player.
     *
     * @return A list containing doubles that you have to add on to a location.
     */

    public static List<double[]> getParticleCoords() {
        List<double[]> coords = new ArrayList<>();
        double t = 0;
        double r = 0.5;
        for (int i = 0; i < 8; i++) {
            t += Math.PI / 4;
            double x = r * Math.cos(t);
            double y = 0.2;
            double z = r * Math.sin(t);
            coords.add(new double[]{x, y, z});
        }
        r = 0.7;
        t = 0;
        for (int i = 0; i < 8; i++) {
            t += Math.PI / 4;
            double x = r * Math.cos(t);
            double y = 1;
            double z = r * Math.sin(t);
            coords.add(new double[]{x, y, z});
        }
        t = 0;
        r = 0.5;
        for (int i = 0; i < 8; i++) {
            t += Math.PI / 4;
            double x = r * Math.cos(t);
            double y = 1.8;
            double z = r * Math.sin(t);
            coords.add(new double[]{x, y, z});
        }
        return coords;
    }
}
