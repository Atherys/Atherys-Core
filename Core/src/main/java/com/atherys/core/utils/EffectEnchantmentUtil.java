package com.atherys.core.utils;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import de.tr7zw.itemnbtapi.NBTItem;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerItemDamageEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.util.Vector;

import java.util.*;

public class EffectEnchantmentUtil implements Listener {

    public static List<String> events = Arrays.asList(
            "onPlayerBreakBlock",
            "onPlayerItemDamage",
            "onPlayerMove",
            "onPlayerTakeDamage",
            "onPlayerDoDamage"
    );

    private static Map<EquipmentSlot,Vector> locationModifiers = new HashMap<>();

    public static EffectEnchantmentUtil instance;
    public static JsonParser parser;
    public static Random random;

    private EffectEnchantmentUtil(JavaPlugin plugin) {
        parser = new JsonParser();
        random = new Random();

        locationModifiers.put(EquipmentSlot.HAND, new Vector(0,0,0) );
        locationModifiers.put(EquipmentSlot.OFF_HAND, new Vector(0,0,0) );
        locationModifiers.put(EquipmentSlot.HEAD, new Vector(0.0d, 1.7d, 0.0d) );
        locationModifiers.put(EquipmentSlot.CHEST, new Vector(0.0d, 1.2d, 0.0d) );
        locationModifiers.put(EquipmentSlot.LEGS, new Vector(0.0d, 0.6d, 0.0d) );
        locationModifiers.put(EquipmentSlot.FEET, new Vector(0.0d, 0.1d, 0.0d) );

        Bukkit.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler
    public void particleEffectPlayerBlockDestroy (BlockBreakEvent event) {
        if ( event.getBlock() == null ) return;
        checkForAndExecuteEffects(event.getPlayer(), "onPlayerBreakBlock", event.getBlock().getLocation() );
    }

    @EventHandler
    public void particleEffectPlayerItemDamage (PlayerItemDamageEvent event ) {
        if ( event.getPlayer() == null ) return;
        checkForAndExecuteEffects(event.getPlayer(), "onPlayerItemDamage", event.getPlayer().getEyeLocation() );
    }

    @EventHandler
    public void particleEffectPlayerMove (PlayerMoveEvent event) {
        if ( event.getTo() == null || event.getFrom() == null ) return;
        if ( event.getTo().getBlockX() == event.getFrom().getBlockX() &&
             event.getTo().getBlockY() == event.getFrom().getBlockY() &&
             event.getTo().getBlockZ() == event.getFrom().getBlockZ() ) return;
        if ( event.getPlayer() == null ) return;
        checkForAndExecuteEffects(event.getPlayer(), "onPlayerMove", event.getPlayer().getLocation() );
    }

    @EventHandler
    public void particleEffectPlayerDamage (EntityDamageByEntityEvent event) {
        if ( event.getDamager() == null || event.getEntity() == null ) return;
        if ( !( event.getDamager() instanceof Player ) ) return;
        if ( !( event.getEntity() instanceof Player ) ) return;

        Player victim = (Player) event.getEntity();
        Player damager = (Player) event.getDamager();

        checkForAndExecuteEffects(victim,  "onPlayerTakeDamage", victim.getLocation() );
        checkForAndExecuteEffects(damager, "onPlayerDoDamage",   damager.getLocation());
    }

    private void checkForAndExecuteEffects ( Player player, String eventType, Location loc ) {

        for ( EquipmentSlot slot : EquipmentSlot.values() ) {
            ItemStack item = player.getInventory().getItem(slot);
            if ( item == null ) continue;

            String effects = NBTItem.getNBTItem(item).getString("SpecialFX");
            if ( !effects.isEmpty() ) {
                JsonObject fx = parser.parse(effects).getAsJsonObject();
                if ( !fx.get("event").getAsString().equals(eventType) ) continue;
                float chance = fx.get("chance").getAsFloat();
                if ( chance <= random.nextFloat() ) continue;

                loc = loc.clone().add(locationModifiers.get(slot));
                if ( slot.equals(EquipmentSlot.HAND) ) {
                    loc = getRightSide(player.getEyeLocation(), 0.45).subtract(0, .6, 0);
                } else if ( slot.equals(EquipmentSlot.OFF_HAND) ) {
                    loc = getLeftSide(player.getEyeLocation(), 0.45).subtract(0, .6, 0);
                }

                if ( fx.has("ParticleFX") ) {
                    JsonObject particleFX = fx.get("ParticleFX").getAsJsonObject();

                    Particle particle = Particle.valueOf(particleFX.get("effect").getAsString());
                    int count = particleFX.get("count").getAsInt();

                    player.getLocation().getWorld().spawnParticle( particle, loc, count, 0.2d, 0.2d, 0.2d, 0.01d );
                }

                if ( fx.has("SoundFX") ) {
                    JsonObject particleFX = fx.get("SoundFX").getAsJsonObject();

                    Sound sound = Sound.valueOf(particleFX.get("sound").getAsString());

                    player.getLocation().getWorld().playSound(loc, sound, 0.02f, random.nextFloat() );
                }

            }
        }
    }

    public static EffectEnchantmentUtil initialize(JavaPlugin plugin) {
        if ( instance == null ) instance = new EffectEnchantmentUtil(plugin);
        return instance;
    }

    public static class EffectEnchantment {

        private Particle particle;
        private Sound sound;

        private String event;
        private int count;
        private float chance;

        public EffectEnchantment ( String event, float chance ) {
            this.event = event;
            this.chance = chance;
        }

        public EffectEnchantment ( Particle particle, int count, Sound sound, String event, float chance ) {
            this.particle = particle;
            this.sound = sound;
            this.count = count;
            this.event = event;
            this.chance = chance;
        }

        public EffectEnchantment ( Particle particle, int count, String event, float chance ) {
            this.particle = particle;
            this.count = count;
            this.event = event;
            this.chance = chance;
        }

        public EffectEnchantment ( Sound sound, String event, float chance ) {
            this.sound = sound;
            this.event = event;
            this.chance = chance;
        }

        public ItemStack applyTo ( ItemStack item ) {
            NBTItem nbtItem = NBTItem.getNBTItem(item);

            nbtItem.setString( "SpecialFX", this.toJson().toString() );

            return nbtItem.getItem();
        }

        public JsonObject toJson() {

            JsonObject FX = new JsonObject();
            FX.addProperty("chance", chance);
            FX.addProperty("event", event);

            if ( particle != null ) {
                JsonObject particleFX = new JsonObject();

                particleFX.addProperty("effect", particle.toString());
                particleFX.addProperty("count", count);

                FX.add("ParticleFX", particleFX);
            }

            if ( sound != null ) {
                JsonObject soundFX = new JsonObject();
                soundFX.addProperty("sound", sound.toString());
                FX.add("SoundFX", soundFX);
            }
            return FX;
        }

        public float getChance() {
            return chance;
        }

        public void setChance(float chance) {
            this.chance = chance;
        }

        public int getParticleCount() {
            return count;
        }

        public void setParticleCount(int count) {
            this.count = count;
        }

        public String getEvent() {
            return event;
        }

        public void setEvent(String event) {
            this.event = event;
        }

        public Sound getSound() {
            return sound;
        }

        public void setSound(Sound sound) {
            this.sound = sound;
        }

        public Particle getParticle() {
            return particle;
        }

        public void setParticle(Particle particle) {
            this.particle = particle;
        }
    }

    public boolean hasItemEffectEnchantment ( ItemStack item ) {
        NBTItem nbtItem = NBTItem.getNBTItem(item);

        if ( nbtItem.hasKey("SpecialFX") ) {
            // Already has other effects applied
            return true;
        } else {
            // Does not have any other effects
            return false;
        }
    }

    public void removeEffectEnchantment ( ItemStack item ) {
        NBTItem nbtItem = NBTItem.getNBTItem(item);
        nbtItem.removeKey("SpecialFX");
    }

    public static EffectEnchantment createEffectEnchantmentFromJson ( JsonObject fx ) {

        EffectEnchantment ench = new EffectEnchantment(fx.get("event").getAsString(), fx.get("chance").getAsFloat());

        JsonObject particleFX = fx.getAsJsonObject("ParticleFX");
        if ( particleFX != null ) {
            ench.setParticle(Particle.valueOf(particleFX.get("effect").getAsString()));
            ench.setParticleCount(particleFX.get("count").getAsInt());
        }

        JsonObject soundFX = fx.getAsJsonObject("SoundFX");
        if ( soundFX != null ) {
            ench.setSound( Sound.valueOf(soundFX.get("sound").getAsString()) );
        }

        return ench;
    }

    public static EffectEnchantment getEffectEnchantment(ItemStack item) {
        NBTItem nbtItem = NBTItem.getNBTItem(item);

        if ( nbtItem.hasKey("SpecialFX") ) {
            JsonObject fx = parser.parse(nbtItem.getString("SpecialFX")).getAsJsonObject();
            return createEffectEnchantmentFromJson(fx);
        } else {
            return null;
        }
    }

    public static Location getRightSide(Location location, double distance) {
        float angle = location.getYaw() / 60;
        return location.clone().subtract(new Vector(Math.cos(angle), 0, Math.sin(angle)).normalize().multiply(distance));
    }

    public static Location getLeftSide(Location location, double distance) {
        float angle = location.getYaw() / 60;
        return location.clone().add(new Vector(Math.cos(angle), 0, Math.sin(angle)).normalize().multiply(distance));
    }
}
