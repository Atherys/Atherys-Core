package com.atherys.core.utils;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.PotionData;
import org.bukkit.potion.PotionType;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class SerializationUtil {

    public static JsonObject itemToJson (ItemStack item ) {
        JsonObject itemJson = new JsonObject();

        itemJson.addProperty("type", item.getType().toString() ); // Material to String

        if ( item.getAmount() > 1 ) itemJson.addProperty("amount", item.getAmount() );

        if ( !item.hasItemMeta() ) return itemJson;

        ItemMeta meta = item.getItemMeta();
        JsonObject itemMeta = new JsonObject();

        if ( meta.hasDisplayName() ) itemMeta.addProperty("name", meta.getDisplayName());
        if ( meta.hasLore() ) {
            JsonArray loreJson = new JsonArray();
            for ( String line : meta.getLore() ) {
                loreJson.add(new JsonPrimitive(line));
            }
            itemMeta.add("lore", loreJson);
        }
        if ( meta.hasEnchants() ) {
            JsonArray enchantJson = new JsonArray();
            Map<Enchantment, Integer> enchants = item.getEnchantments();
            for ( Map.Entry<Enchantment, Integer> entry : enchants.entrySet() ) {
                JsonObject enchant = new JsonObject();
                enchant.addProperty("id", entry.getKey().getName()); // Enchantment to Name
                enchant.addProperty("lvl", entry.getValue());
                enchantJson.add(enchant);
            }
            itemMeta.add("enchants", enchantJson);
        }

        if ( meta instanceof LeatherArmorMeta ) {
            LeatherArmorMeta armorMeta = (LeatherArmorMeta) meta;
            JsonObject armorMetaJson = new JsonObject();
            armorMetaJson.addProperty("color", armorMeta.getColor().asRGB());
            itemMeta.add("armorMeta", armorMetaJson);
        }

        if ( meta instanceof BookMeta ) {
            BookMeta bookMeta = (BookMeta) meta;

            JsonObject bookMetaJson = new JsonObject();

            bookMetaJson.addProperty("title", bookMeta.getTitle());
            bookMetaJson.addProperty("author", bookMeta.getAuthor());

            JsonArray pages = new JsonArray();

            for ( String page : bookMeta.getPages() ) {
                pages.add( new JsonPrimitive(page) );
            }
            bookMetaJson.add("pages", pages);

            itemMeta.add("bookMeta", bookMetaJson );
        }

        if ( meta instanceof PotionMeta) {
            PotionMeta potMeta = (PotionMeta) meta;
            JsonObject potionMetaJson = new JsonObject();

            potionMetaJson.addProperty("type", potMeta.getBasePotionData().getType().toString() ); // PotionType to String
            potionMetaJson.addProperty("extended", potMeta.getBasePotionData().isExtended() );
            potionMetaJson.addProperty("upgraded", potMeta.getBasePotionData().isUpgraded() );

            itemMeta.add("potionMeta", potionMetaJson);
        }

        itemJson.add("meta", itemMeta);

        EffectEnchantmentUtil.EffectEnchantment effectEnchantment = EffectEnchantmentUtil.getEffectEnchantment(item);
        if ( effectEnchantment != null ) {
            itemJson.add("SpecialFX", effectEnchantment.toJson() );
        }

        return itemJson;
    }

    public static ItemStack jsonToItem ( JsonObject json ) {

        if ( !json.has("type") ) try {
            throw new MalformedItemException(json.toString() + " does not contain the 'type' tag.");
        } catch (MalformedItemException e) {
            e.printStackTrace();
        }

        Material mat = Material.getMaterial( json.get("type").getAsString() );

        ItemStack item = new ItemStack( mat, 1 );

        if ( json.has("amount") ) item.setAmount( json.get("amount").getAsInt() );

        if ( !json.has("meta") ) return item;

        JsonObject meta = json.get("meta").getAsJsonObject();
        ItemMeta itemMeta = item.getItemMeta();

        if ( meta.has("name") ) itemMeta.setDisplayName( ChatColor.translateAlternateColorCodes('&', meta.get("name").getAsString() ) );
        if ( meta.has("lore") ) {
            JsonArray lore = meta.get("lore").getAsJsonArray();
            List<String> actualLore = new ArrayList<>();
            for (int i = 0; i < lore.size(); i++) {
                actualLore.add(i, ChatColor.translateAlternateColorCodes('&', lore.get(i).getAsString()));
            }
            itemMeta.setLore(actualLore);
        }

        if ( meta.has("armorMeta") ) {
            LeatherArmorMeta armorMeta = (LeatherArmorMeta) itemMeta;
            ((LeatherArmorMeta) itemMeta).setColor(Color.fromRGB(meta.getAsJsonObject("armorMeta").get("color").getAsInt()) );
            item.setItemMeta(armorMeta);
        }

        if ( meta.has("bookMeta") ) {
            BookMeta bookMeta = (BookMeta) itemMeta;
            JsonObject jsonBookMeta = meta.get("bookMeta").getAsJsonObject();

            bookMeta.setTitle( jsonBookMeta.get("title").getAsString() );
            bookMeta.setAuthor( jsonBookMeta.get("author").getAsString());

            JsonArray pages = jsonBookMeta.getAsJsonArray("pages");
            for ( int i = 0; i < pages.size(); i++ ) {
                bookMeta.addPage( pages.get(i).getAsString() );
            }

            item.setItemMeta(bookMeta);
            return item;
        }

        if ( meta.has("potionMeta") ) {
            PotionMeta potionMeta = (PotionMeta) itemMeta;
            JsonObject jsonPotionMeta = meta.get("potionMeta").getAsJsonObject();

            PotionData data = new PotionData( PotionType.valueOf( jsonPotionMeta.get("type").getAsString() ), jsonPotionMeta.get("extended").getAsBoolean(), jsonPotionMeta.get("upgraded").getAsBoolean() );

            potionMeta.setBasePotionData(data);

            item.setItemMeta(potionMeta);
            return item;
        }

        item.setItemMeta( itemMeta );

        if ( meta.has("enchants") ) {
            JsonArray enchants = meta.get("enchants").getAsJsonArray();
            for ( JsonElement enchant : enchants ) {
                Enchantment ench = Enchantment.getByName(enchant.getAsJsonObject().get("id").getAsString());
                int level = enchant.getAsJsonObject().get("lvl").getAsInt();
                item.addUnsafeEnchantment(ench, level);
            }
            itemMeta.getItemFlags().remove(ItemFlag.HIDE_ENCHANTS);
        }

        if ( json.has("SpecialFX") ) {
            EffectEnchantmentUtil.EffectEnchantment effectEnchantment = EffectEnchantmentUtil.createEffectEnchantmentFromJson(json.getAsJsonObject("SpecialFX"));
            if ( effectEnchantment != null ) {
                effectEnchantment.applyTo(item);
            }
        }

        return item;
    }

    private static class MalformedItemException extends Exception {
        public MalformedItemException() {}

        public MalformedItemException(String message)
        {
            super(message);
        }
    }

}
