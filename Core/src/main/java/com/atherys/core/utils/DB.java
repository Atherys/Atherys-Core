package com.atherys.core.utils;

import java.io.File;
import java.sql.*;

public class DB {
    private final String url;

    public DB(String host, int port, String database, String user, String password) {
        url = "jdbc:mysql://" + host + ":" + port + "/" + database + "?user=" + user + "&password=" + password + "&useSSL=false";
        initDriver("com.mysql.jdbc.Driver");
    }

    public DB(String path) {
        url = "jdbc:sqlite:" + new File(path).getAbsolutePath();
        initDriver("org.sqlite.JDBC");
    }

    private void initDriver(final String driver) {
        try {
            Class.forName(driver);
        } catch (Exception ex) {
            System.out.println("Database Driver Error:" + ex.getMessage());
        }
    }

    public ResultSet query(final String query) {
        try {
            Connection connection = DriverManager.getConnection(url);
            PreparedStatement statement = connection.prepareStatement(query);
            if (statement.execute())return statement.getResultSet();
        } catch (SQLException ex) {
            System.out.println("Database Error: "+ex.getMessage());
        }
        return null;
    }
}
