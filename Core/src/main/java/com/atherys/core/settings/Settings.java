package com.atherys.core.settings;

import com.atherys.core.Core;
import org.bukkit.ChatColor;
import org.bukkit.configuration.Configuration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author apteryx
 * @time 9:54 AM
 * @since 12/29/2016
 */
public class Settings {

    private final Configuration config;
    private String PVEServerName;

    public Settings() {
        config = Core.getInstance().getConfig();
    }

    public boolean checkChorusWorld(String worldName) {
        for (String world : config.getStringList("chorus-fruit.worlds")) {
            if (world.equalsIgnoreCase(worldName)) {
                return true;
            }
        }
        return false;
    }

    public boolean canEnderPearl() {
        return config.getBoolean("pearl-teleport");
    }

    public boolean isPlayerVersusEntityWorld(String worldName) {
        for (String world : config.getStringList("player-versus-entity.worlds")) {
            if (world.equalsIgnoreCase(worldName)) {
                return true;
            }
        }
        return false;
    }

    public String getMaintenanceMOTD() {
        return ChatColor.translateAlternateColorCodes('&', config.getString("maintenance-motd"));
    }

    public boolean disableCombatBoats() {
        return config.getBoolean("disable-combat-boats");
    }

    public List<String> getJoinCommands() {
        return config.getStringList("first-join-commands");
    }

    public long getToggleCooldown() {
        return config.getLong("pvptoggle-cooldown");
    }

    public List<String> getServerMessages () {
        return config.getStringList("announcer-messages");
    }

    public long getServerMessageDelay () {
        return config.getLong("announcer-delay");
    }

    public long getClassChangeCooldown() { return config.getLong("class-change-cooldown"); }

    public Map<String,String> getDatabaseInfo() {
        Map<String,String> dbInfo = new HashMap<>();

        dbInfo.put("sql",config.getString("sql.type"));
        dbInfo.put("ip",config.getString("sql.ip"));
        dbInfo.put("port",config.getString("sql.port"));
        dbInfo.put("db",config.getString("sql.db"));
        dbInfo.put("username",config.getString("sql.username"));
        dbInfo.put("password",config.getString("sql.password"));

        return dbInfo;
    }

    public long getPvpToggleCooldown() { return config.getLong("pvptoggle-cooldown"); }

    public boolean getTabBlockCompleteEnabled() { return config.getBoolean("block-tab-complete"); }

    public String getPartyColor() { return config.getString("party-color"); }

    public String getMainServerName() {
        return config.getString("main-server.name");
    }

    public String getPVEServerName() {
        return config.getString("pve-server.name");
    }

    public boolean isPVENetherPortalEnabled() {
        return config.getBoolean("pve-server.enable-nether-portal");
    }
}
