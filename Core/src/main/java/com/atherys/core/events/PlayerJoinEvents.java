package com.atherys.core.events;

import com.atherys.core.Core;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

/**
 * @author apteryx
 * @time 1:08 PM
 * @since 1/9/2017
 */
public class PlayerJoinEvents implements Listener {

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void playerJoinEvent(PlayerJoinEvent event) {
        if (!event.getPlayer().hasPlayedBefore()) {
            for (String command : Core.getInstance().getSettings().getJoinCommands()) {
                command = command.replace("%player%", event.getPlayer().getName());
                Bukkit.dispatchCommand(Bukkit.getConsoleSender(), command);
            }
        }

    }
}
