package com.atherys.core.events;

import com.atherys.core.Core;
import com.herocraftonline.heroes.api.events.HeroJoinPartyEvent;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import java.util.HashSet;

/**
 * @author apteryx
 * @time 12:41 PM
 * @since 2/3/2017
 */
public class HeroPartyEvents implements Listener {

    private HashSet<Scoreboard> scoreboards = new HashSet<>();

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onHeroJoinParty(HeroJoinPartyEvent event) {
        Scoreboard scoreboard = Bukkit.getScoreboardManager().getMainScoreboard();
        if (scoreboard.getTeam(event.getParty().toString().split("@")[1]) != null) {
            Bukkit.getLogger().info("Party ID: " + event.getParty().toString().split("@")[1]);
            Team partyTeam = scoreboard.getTeam(event.getParty().toString().split("@")[1]);
            partyTeam.addEntry(event.getHero().getPlayer().getName());
            partyTeam.setPrefix(Core.getInstance().getSettings().getPartyColor());
        }
    }

}
