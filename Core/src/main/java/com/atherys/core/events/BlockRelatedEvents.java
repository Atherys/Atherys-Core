package com.atherys.core.events;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPistonExtendEvent;
import org.bukkit.event.block.BlockPistonRetractEvent;

/**
 * @author apteryx
 * @time 11:12 AM
 * @since 12/29/2016
 */
public class BlockRelatedEvents implements Listener {

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void unbreakableBlockBreakEvent(BlockBreakEvent event) {
        if (event.getBlock().hasMetadata("HeroesUnbreakableBlock")) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void unbreakablePistonExtendEvent(BlockPistonExtendEvent event) {
        event.getBlocks().stream().filter(block -> block.hasMetadata("HeroesUnbreakableBlock")).forEach(block -> event.setCancelled(true));
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void unbreakablePistonRetractEvent(BlockPistonRetractEvent event) {
        event.getBlocks().stream().filter(block -> block.hasMetadata("HeroesUnbreakableBlock")).forEach(block -> event.setCancelled(true));
    }

}
