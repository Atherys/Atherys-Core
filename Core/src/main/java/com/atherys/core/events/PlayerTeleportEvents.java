package com.atherys.core.events;

import com.atherys.core.Core;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerPortalEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

/**
 * @author apteryx
 * @time 9:48 AM
 * @since 12/29/2016
 */
public class PlayerTeleportEvents implements Listener {

    private static int millisecondsToWait = 10000;

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void  playerTeleportEvent(PlayerTeleportEvent event) {
        if (event.getCause() == PlayerTeleportEvent.TeleportCause.CHORUS_FRUIT) {
            if (Core.getInstance().getSettings().checkChorusWorld(event.getPlayer().getWorld().getName())) {
                event.setCancelled(true);
            }
        }

        if (event.getCause() == PlayerTeleportEvent.TeleportCause.ENDER_PEARL) {
            if (!Core.getInstance().getSettings().canEnderPearl()) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void playerNetherEnterEvent (PlayerPortalEvent event) {
        if ( event.getCause().equals(PlayerTeleportEvent.TeleportCause.NETHER_PORTAL) && Core.getInstance().getSettings().isPVENetherPortalEnabled() ) {

            Player player = event.getPlayer();

            String server = getBungeeServerName();

            if ( server == null ) {
                player.sendMessage(ChatColor.RED + "An internal error has occured. Please give the following information to an admin:\n" + Bukkit.getServerName() );
                return;
            }

            String serverToTeleportTo;

            if ( server.equals(Core.getInstance().getSettings().getPVEServerName()) ) {
                serverToTeleportTo = Core.getInstance().getSettings().getMainServerName();
            } else {
                serverToTeleportTo = Core.getInstance().getSettings().getPVEServerName();
            }

            if ( Core.checkInCombat(player) ) return;

            informPlayerTeleportToMisery(player, server);

            player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, millisecondsToWait/50, 100, false, false ));
            Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(Core.getInstance(), () -> Core.connectPlayerToServer(player, serverToTeleportTo), millisecondsToWait / 50);
        }
    }

    private static void informPlayerTeleportToMisery ( Player player, String server ) {


        System.out.println( "Player " + player.getName() + " teleporting to server " + server + " ( " + Bukkit.getServerName() + " )" );

        String loreName;

        if ( server.equals(Core.getInstance().getSettings().getPVEServerName()) ) {
            loreName = "the world of A'therys";
        } else {
            loreName = "the island of Misery";
        }

        player.sendMessage(ChatColor.GREEN + "" + ChatColor.BOLD + "Teleporting you to " + loreName + " in " + millisecondsToWait / 1000 + " seconds.");

        for (Entity e : player.getNearbyEntities(15, 15, 15)) {
            if (e instanceof Player) {
                e.sendMessage(ChatColor.GREEN + "" + ChatColor.BOLD + player.getName() + " will be teleported to "  + loreName + " in " + millisecondsToWait / 1000 + " seconds.");
            }
        }
    }

    private static String getBungeeServerName () {
        switch ( Bukkit.getServerName() ) {
            case "Atherys":
                return Core.getInstance().getSettings().getMainServerName();
            case "PvE Server":
                return Core.getInstance().getSettings().getPVEServerName();
        }
        return null;
    }

}
