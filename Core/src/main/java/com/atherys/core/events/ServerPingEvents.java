package com.atherys.core.events;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.server.ServerListPingEvent;

/**
 * @author apteryx
 * @time 5:16 PM
 * @since 1/16/2017
 */
public class ServerPingEvents implements Listener {

    @EventHandler
    public void serverPingEvent(ServerListPingEvent event) {
        // if server is in maintenance mode, set motd to maintenance motd
    }
}
