package com.atherys.core.events;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;

/**
 * Created by pumpapa on 13/01/16.
 */
public class MobSpawnListener implements Listener {

    public MobSpawnListener() {}

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onMobSpawn(CreatureSpawnEvent event) {
        if (event.getEntity() == null || !event.getLocation().getWorld().getName().equalsIgnoreCase("PVE")) {
            return;
        }
        if (event.getSpawnReason() == CreatureSpawnEvent.SpawnReason.NATURAL || event.getSpawnReason() == CreatureSpawnEvent.SpawnReason.SPAWNER ||
                event.getSpawnReason() == CreatureSpawnEvent.SpawnReason.REINFORCEMENTS || event.getSpawnReason() == CreatureSpawnEvent.SpawnReason.VILLAGE_INVASION) {
            event.setCancelled(true);
        }
    }
}
