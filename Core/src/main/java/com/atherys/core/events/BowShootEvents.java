package com.atherys.core.events;

import com.atherys.core.Core;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.characters.Hero;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.metadata.FixedMetadataValue;

/**
 * @author apteryx
 * @time 1:03 PM
 * @since 1/26/2017
 */
public class BowShootEvents implements Listener {

    @EventHandler
    public void onBowFire(EntityShootBowEvent event) {
        if (event.getEntity() instanceof Player) {
            Hero hero = Heroes.getInstance().getCharacterManager().getHero((Player) event.getEntity());
            if (hero != null) {
                event.getProjectile().setMetadata("BowForce", new FixedMetadataValue(Core.getInstance(), event.getForce()));
            }
        }
    }
}
