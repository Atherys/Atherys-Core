package com.atherys.core.events;

import com.atherys.core.Core;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.characters.Hero;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

/**
 * @author apteryx
 * @time 12:57 PM
 * @since 1/9/2017
 */
public class PlayerInteractEvents implements Listener {

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void playerInteractEvent(PlayerInteractEvent event) {
        if (event.getItem() != null) {
            if (event.getItem().getType() == Material.BOAT) {
                if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
                    Hero hero = Heroes.getInstance().getCharacterManager().getHero(event.getPlayer());
                    if (hero != null) {
                        if (hero.isInCombat()) {
                            if (Core.getInstance().getSettings().disableCombatBoats()) {
                                event.getPlayer().sendMessage("\247CYou can't place boats while in combat.");
                                event.setCancelled(true);
                            }
                        }
                    }
                }
            }
        }
    }
}
