package com.atherys.core.events;

import com.atherys.core.Core;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.characters.CharacterDamageManager;
import com.herocraftonline.heroes.characters.Hero;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;

/**
 * @author apteryx
 * @time 12:44 PM
 * @since 1/9/2017
 */
public class DamageRelatedEvents implements Listener {

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void entityDamageEntity(EntityDamageByEntityEvent event) {
        if (event.getDamager() instanceof Player && event.getEntity() instanceof Player) {
            if (Core.getInstance().getSettings().isPlayerVersusEntityWorld(event.getDamager().getWorld().getName())) {
                event.setCancelled(true);
            }
        }

        if (event.getDamager() instanceof Arrow) {
            Projectile arrow = (Projectile) event.getDamager();
            if (arrow.getShooter() instanceof Player) {
                Hero hero = Heroes.getInstance().getCharacterManager().getHero((Player) arrow.getShooter());
                if (hero != null) {
                    if (arrow.hasMetadata("BowForce")) {
                        double damage = hero.getHeroClass().getProjectileDamage(CharacterDamageManager.ProjectileType.ARROW).getScaled(hero) * arrow.getMetadata("BowForce").get(0).asFloat();
                        event.setDamage(damage);
                    }
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void entityDamageEvent(EntityDamageEvent event) {
        if (event.getEntity() instanceof Player) {
            Player player = (Player) event.getEntity();
            if (event.getCause() == EntityDamageEvent.DamageCause.SUFFOCATION) {
                if (player.getEyeLocation().getBlock().hasMetadata("HeroesUnbreakableBlock") || player.getLocation().getBlock().hasMetadata("HeroesUnbreakableBlock")) {
                    event.setCancelled(true);
                }
            }
        }
    }
}
