package com.atherys.core.announcer;

import com.atherys.core.Core;
import com.atherys.core.commands.AnnouncerReloadCommand;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.Collection;
import java.util.List;
import java.util.Random;

public class Announcer {

    private static Announcer instance = new Announcer();

    private List<String> messages;
    private Random random;
    private long delay;

    private int schedulerId = -1;

    private Announcer () {

        if ( schedulerId != -1 ) {
            Bukkit.getScheduler().cancelTask(schedulerId);
        }

        this.delay = Core.getInstance().getSettings().getServerMessageDelay();
        this.messages = Core.getInstance().getSettings().getServerMessages();
        this.random = new Random();

        Bukkit.getLogger().info( "[AtherysCore] Announcer loaded " + messages.size() + " messages." );

        schedulerId = Bukkit.getScheduler().scheduleSyncRepeatingTask(Core.getInstance(), () -> {
            int r = random.nextInt( messages.size() );
            Collection<? extends Player> players = Bukkit.getServer().getOnlinePlayers();
            String msg = messages.get(r);
            for ( Player player : players ) {
                player.sendMessage(ChatColor.translateAlternateColorCodes('&', msg));
            }
        }, 0, (delay / 50));

        new AnnouncerReloadCommand();
    }


    public int getSchedulerId() {
        return schedulerId;
    }

    public static Announcer getInstance() { return instance; }

    public void reload() {
        instance = new Announcer();
    }
}
