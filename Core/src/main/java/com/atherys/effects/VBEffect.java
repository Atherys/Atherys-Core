package com.atherys.effects;

import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.skill.Skill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import java.util.HashSet;
import java.util.Set;

public class VBEffect extends PeriodicExpirableEffect {
    private final int radius;
    private final Block b;
    private final long md;
    private double damage;
    private String applyText;
    private String expireText;
    private Set<Block> k = new HashSet<>();
    private Skill skill;

    public VBEffect(Skill skill, int period, int duration, double damage, int radius, Block b, long md,Hero hero) {
        super(skill, "VBEffect",hero.getPlayer(), period, duration);
        this.skill = skill;
        this.radius = radius;
        this.b = b;
        this.damage = damage;
        this.md = md;
        this.applyText = SkillConfigManager.getRaw(skill, SkillSetting.APPLY_TEXT, "%hero% used %skill%").replace("%hero%", "$1").replace("%skill%", "$2");
        this.expireText = SkillConfigManager.getRaw(skill, SkillSetting.EXPIRE_TEXT, "%hero%'s %skill% expired.").replace("%hero%", "$1").replace("%skill%", "$2");
        types.add(EffectType.DISPELLABLE);
        types.add(EffectType.BENEFICIAL);
        types.add(EffectType.FORM);
    }

    @Override
    public void applyToHero(Hero hero) {
        super.applyToHero(hero);
        broadcast(hero.getPlayer().getLocation(), applyText, hero.getName(), "VrovonasBoon");
    }

    @Override
    public void removeFromHero(Hero hero) {
        super.applyToHero(hero);
        Set<Block> u = new HashSet<>(k);
        for (Block b : u)
            b.setType(Material.AIR);
        k.clear();
        if (this.isExpired()) {
            broadcast(hero.getPlayer().getLocation(), expireText, hero.getName(), "VrovonasBoon");
        }
    }

    @Override
    public void tickMonster(Monster mnstr) {
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void tickHero(Hero hero) {
        for (Entity e : b.getLocation().getWorld().getNearbyEntities(b.getLocation(), radius, radius, radius)) {
            if (!(e instanceof Player)) continue;
            Hero h = plugin.getCharacterManager().getHero((Player) e);
            if (!h.hasEffect("Might") && (h.equals(hero) || h.hasParty() && (h.getParty().getMembers().contains(hero)))) {
                h.addEffect(new MightEffect(skill, md, damage,h));
            }
        }
    }
}

