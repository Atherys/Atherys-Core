package com.atherys.effects;

import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.Skill;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

/**
 * Created by pumpapa on 02/11/15.
 */
public class BulwarkEffect extends ExpirableEffect {
    private double health;
    private int absorptionLevel;
    private double absorption;
    private double damageBlocked = 0;

    public BulwarkEffect(Skill skill, long duration, double health, Hero hero) {
        super(skill, "Bulwark",hero.getPlayer(), duration);
        this.health = health;
        this.absorptionLevel = 4;
        this.absorption = health/5;
        addMobEffect(22, (int)(duration/50), 4, false);
        types.add(EffectType.BENEFICIAL);
    }

    public double damage(double damage, Player player) {
        if (health - damage < 0) {
            Messaging.send(player, "Your shield blocked $1 damage!", (int)(damageBlocked+health));
            this.expire();
            return damage - health;
        }
        damageBlocked += damage;
        if (damage > 1) {
            Messaging.send(player, "Your shield blocked $1 damage!", (int)damageBlocked);
        }
        health -= damage;
        int level = (int)(health/absorption);
        if (level < absorptionLevel) {
            player.addPotionEffect(new PotionEffect(PotionEffectType.ABSORPTION, (int) (getRemainingTime() / 50), level, false, false), true);
            absorptionLevel = level;
        }
        return 0;
    }

    @Override
    public void applyToHero(Hero hero) {
        super.applyToHero(hero);
        Messaging.send(hero.getPlayer(), "You gained a temporary shield.");
    }

    @Override
    public void removeFromHero(Hero hero) {
        super.removeFromHero(hero);
        Messaging.send(hero.getPlayer(), "Your shield ran out!");
        hero.getPlayer().removePotionEffect(PotionEffectType.ABSORPTION);
    }
}
