package com.atherys.effects;

import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.Skill;
import com.herocraftonline.heroes.util.Messaging;

/**
 * Created by pumpapa on 30/10/15.
 */
public class DamageReductionEffect extends ExpirableEffect {
    private double damageReduction;
    private String applyText;
    private String expireText;

    public DamageReductionEffect(Skill skill, long duration, double damageReduction, String applyText, String expireText,Hero hero) {
        super(skill, "DamageReduction",hero.getPlayer(), duration);
        this.damageReduction = damageReduction;
        this.applyText = applyText;
        this.expireText = expireText;
        this.types.add(EffectType.BENEFICIAL);
        this.types.add(EffectType.PHYSICAL);
    }

    @Override
    public void applyToHero(Hero hero) {
        super.applyToHero(hero);
        if (!applyText.isEmpty()) {
            Messaging.send(hero.getPlayer(), applyText);
        }
    }

    @Override
    public void removeFromHero(Hero hero) {
        super.removeFromHero(hero);
        if (!expireText.isEmpty()) {
            Messaging.send(hero.getPlayer(), expireText);
        }
    }

    public double getDamageReduction() {
        return damageReduction;
    }
}
