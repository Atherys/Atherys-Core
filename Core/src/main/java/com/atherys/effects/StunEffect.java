package com.atherys.effects;

import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.skill.Skill;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.Location;

public class StunEffect extends PeriodicExpirableEffect {
    private Location loc;

    public StunEffect(Skill skill, long duration,Hero hero) {
        super(skill, "Stun",hero.getPlayer(),100L, duration);
        this.types.add(EffectType.STUN);
        this.types.add(EffectType.HARMFUL);
        this.types.add(EffectType.PHYSICAL);
        this.types.add(EffectType.DISABLE);
        this.addMobEffect(9, (int) (duration / 1000L) * 20, 127, false);
    }

    public void applyToHero(Hero hero) {
        super.applyToHero(hero);
        Messaging.send(hero.getPlayer(), "You are stunned!");
        this.loc = hero.getPlayer().getLocation();
    }

    public void removeFromHero(Hero hero) {
        super.removeFromHero(hero);
        Messaging.send(hero.getPlayer(), "You are no longer stunned!");
    }

    public void tickHero(Hero hero) {
        Location location = hero.getPlayer().getLocation();
        if (location != null) {
            if (location.getX() != this.loc.getX() || location.getY() != this.loc.getY() || location.getZ() != this.loc.getZ()) {
                this.loc.setYaw(location.getYaw());
                this.loc.setPitch(location.getPitch());
                hero.getPlayer().teleport(this.loc);
            }

        }
    }

    public void tickMonster(Monster monster) {
    }
}
