package com.atherys.effects;

import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.Skill;
import org.bukkit.entity.Player;

/**
 * Created by computer on 9/14/2015.
 */
public class VenomMarkEffect extends ExpirableEffect{
    Player caster;

    public VenomMarkEffect(Skill skill, long duration, Player caster){
        super(skill, "SparkStackEffect",caster,duration);
        this.caster = caster;
    }

    public Player getCaster(){
        return caster;
    }
}
