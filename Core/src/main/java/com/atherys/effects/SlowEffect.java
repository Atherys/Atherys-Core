package com.atherys.effects;

import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.Skill;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.entity.Player;

public class SlowEffect
        extends ExpirableEffect {
    private final String applyText;
    private final String expireText;
    private final Hero applier;

    public SlowEffect(Skill skill, String slow, long duration, int amplifier, boolean swing, String applyText, String expireText, Hero applier) {
	    super(skill,"Slow", applier.getPlayer(),duration);
	    this.applyText = applyText;
        this.expireText = expireText;
        this.applier = applier;

        this.types.add(EffectType.DISPELLABLE);
        this.types.add(EffectType.HARMFUL);
        this.types.add(EffectType.SLOW);

        int tickDuration = (int) (duration / 1000L) * 20;
        addMobEffect(2, tickDuration, amplifier, false);
        if (swing) {
            addMobEffect(4, tickDuration, amplifier, false);
        }
    }

    public SlowEffect(Skill skill, long duration, int amplifier, boolean swing, String applyText, String expireText, Hero applier) {
        this(skill, "Slow", duration, amplifier, swing, applyText, expireText, applier);
    }

	public void applyToHero(Hero hero) {
        super.applyToHero(hero);
        Player player = hero.getPlayer();
        broadcast(player.getLocation(), this.applyText, player.getDisplayName(), this.applier.getPlayer().getDisplayName());
    }

    public void removeFromHero(Hero hero) {
        super.removeFromHero(hero);
        Player player = hero.getPlayer();
        broadcast(player.getLocation(), this.expireText, player.getDisplayName());
    }

    public void applyToMonster(Monster monster) {
        super.applyToMonster(monster);
        broadcast(monster.getEntity().getLocation(), this.applyText, monster.getName(), this.applier.getPlayer().getDisplayName());
    }

    public void removeFromMonster(Monster monster) {
        super.removeFromMonster(monster);
        broadcast(monster.getEntity().getLocation(), this.expireText, monster.getName());
    }
}
