package com.atherys.effects;

import com.herocraftonline.heroes.characters.CharacterTemplate;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.Effect;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.skill.Skill;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;


public class CommanderEffect extends PeriodicExpirableEffect {
    /* Makes Hero the "Commander", imparting <skill> to party members in <radius> */
    private final String applyText;
    private final String expireText;
    private final int radius;
    private Effect effect;

    public CommanderEffect(Skill skill, long period, long duration, int radius, Effect effect, String applyText, String expireText,Hero hero) {
        super(skill, "CommanderEffect",hero.getPlayer(), period, duration);
        this.applyText = applyText;
        this.expireText = expireText;
        this.radius = radius;
        this.effect = effect;
        this.types.add(EffectType.BENEFICIAL);
    }

    public void applyToHero(Hero hero) {
        super.applyToHero(hero);
        Player player = hero.getPlayer();
        broadcast(player.getLocation(), this.applyText, player.getDisplayName());
    }

    public void removeFromHero(Hero hero) {
        super.removeFromHero(hero);
        Player player = hero.getPlayer();
        broadcast(player.getLocation(), this.expireText, player.getDisplayName());
    }

    @Override
    public void tickHero(Hero hero) {
        hero.addEffect(effect);
        if (hero.hasParty()) {
            for (Entity e : hero.getPlayer().getNearbyEntities(this.radius, this.radius, this.radius)) {
                if (e instanceof Player && hero.getParty().isPartyMember((Player) e)) {
                    CharacterTemplate target = this.plugin.getCharacterManager().getCharacter((LivingEntity) e);
                    target.addEffect(this.effect);
                }
            }
        }
    }

    @Override
    public void tickMonster(Monster mob) {
    }
}
