package com.atherys.effects;

import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.skill.Skill;
import org.bukkit.entity.Player;

/**
 * Created by pumpapa on 14/10/15.
 */
public class SkillCastingPeriodicEffect extends PeriodicExpirableEffect {
    private String applyText;
    private String expireText;
    private String interruptionText;
    private boolean slow;

    public SkillCastingPeriodicEffect(Skill skill, long period, long duration, boolean slow, String applyText, String expireText, String interruptionText,Hero hero) {
        super(skill, "SkillCasting",hero.getPlayer(), period, duration);
        this.applyText = applyText;
        this.expireText = expireText;
        this.interruptionText = interruptionText;
        this.slow = slow;
        if (slow) {
            addMobEffect(2, (int)(duration/50), 3, false);
            addMobEffect(4, (int)(duration/50), 254, false);
            this.types.add(EffectType.WEAKNESS);
        }
    }

    public SkillCastingPeriodicEffect(Skill skill, long period, long duration, boolean slow, boolean text,Hero hero) {
	    this(skill,period, duration, slow, text ? "$1 begins channeling $2!" : "", text ? "$1 used $2!" : "", text ? "$1 was interrupted from channeling $2!" : "",hero);
    }



	@Override
    public void applyToHero(Hero hero) {
        super.applyToHero(hero);
        Player player = hero.getPlayer();
        broadcast(player.getLocation(), applyText, new Object[] {player.getDisplayName(), skill.getName()});
    }

    @Override
    public void removeFromHero(Hero hero) {
        super.removeFromHero(hero);
        Player player = hero.getPlayer();
        if(this.isExpired()) {
            broadcast(player.getLocation(), expireText, new Object[] { player.getDisplayName(), skill.getName() });
        } else {
            broadcast(player.getLocation(), interruptionText, new Object[] { player.getDisplayName(), skill.getName() });
        }
    }

    @Override
    public void tickHero(Hero hero) {}

    @Override
    public void tickMonster(Monster monster) {
        //N/A
    }

    public boolean hasSlow() {
        return slow;
    }
}
