package com.atherys.effects;

import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.skill.Skill;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.Particle;
import org.bukkit.entity.Player;

public class SparkStackEffect extends PeriodicExpirableEffect {

    private int stacks;

    public SparkStackEffect(Skill skill, long duration, int stacks,Hero hero) {
        super(skill, "SparkStackEffect",hero.getPlayer(), 1000L, duration);
        this.stacks = stacks;
        this.types.add(EffectType.UNBREAKABLE);
    }

    @Override
    public void tickMonster(Monster mnstr) {
    }

    @Override
    public void tickHero(Hero hero) {
        if(stacks == 3){
            Player player = hero.getPlayer();
            player.getLocation().getWorld().spawnParticle(Particle.VILLAGER_ANGRY, player.getLocation().add(0,1,0), 1 );
        }
    }

    @Override
    public void applyToHero(Hero hero) {
        super.applyToHero(hero);
    }

    @Override
    public void removeFromHero(Hero hero) {
        super.removeFromHero(hero);
        if (this.isExpired()) {
            Messaging.send(hero.getPlayer(), "You no longer have any spark stacks.");
        }
    }

    public void addStack() {
        stacks++;
    }

    public int getStacks() {
        return stacks;
    }

}