package com.atherys.effects;

import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.skill.Skill;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.Particle;
import org.bukkit.entity.Player;

/**
 * Created by pumpapa on 30/10/15.
 */
public class WaveStackEffect extends PeriodicExpirableEffect {
    private int stacks;

    public WaveStackEffect(Skill skill, long duration, int stacks,Hero hero) {
        super(skill, "WaveStack",hero.getPlayer(), 1000L, duration);
        this.stacks = stacks;
        this.types.add(EffectType.UNBREAKABLE);
    }

    public static void addWaveStacks(Skill skill, Hero hero, int stacks) {
        if (hero.hasEffect("WaveStack")) {
            stacks += ((WaveStackEffect) hero.getEffect("WaveStack")).getStacks();
            stacks = stacks > 3 ? 3 : stacks;
        }
        hero.addEffect(new WaveStackEffect(skill, 8000, stacks,hero));
    }

    public static boolean chargeWave(Hero hero) {
        if (hero.hasEffect("WaveStack")) {
            WaveStackEffect waveStackEffect = ((WaveStackEffect) hero.getEffect("WaveStack"));
            if (waveStackEffect.getStacks() == 3) {
                hero.removeEffect(waveStackEffect);
                return true;
            }
        }
        return false;
    }

    public int getStacks() {
        return stacks;
    }

    @Override
    public void tickHero(Hero hero) {
        if (stacks == 3) {
            Player player = hero.getPlayer();
            player.getLocation().getWorld().spawnParticle(Particle.WATER_DROP, player.getLocation().add(0,1,0), 1 );
        }
    }

    @Override
    public void tickMonster(Monster monster) {
        //N/A
    }

    @Override
    public void applyToHero(Hero hero) {
        super.applyToHero(hero);
        if (stacks == 3) {
            Messaging.send(hero.getPlayer(), "Your $1 stacks are all charged up!", "Wave");
        }
    }

    @Override
    public void removeFromHero(Hero hero) {
        super.removeFromHero(hero);
        if (this.isExpired()) {
            Messaging.send(hero.getPlayer(), "You no longer have any $1 stacks.", "Wave");
        }
    }
}