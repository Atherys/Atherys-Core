package com.atherys.effects;

import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.Skill;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.entity.Creature;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

/**
 * Created by NeumimTo, implemented by Saterra.
 * Invis potion effect and Teams
 */
public class BetterInvis extends ExpirableEffect {
    private final String applyText;
    private final String expireText;
    private final Hero caster;

    public BetterInvis(Skill skill, long duration, String applyText, String expireText, Hero hero) {
        super(skill, "Invisible", hero.getPlayer() ,duration);
        this.applyText = applyText;
        this.expireText = expireText;
        this.caster = hero;
        this.types.add(EffectType.BENEFICIAL);
        this.types.add(EffectType.INVIS);
        this.types.add(EffectType.UNTARGETABLE);
        this.types.add(EffectType.MAGIC);
    }

    public void applyToHero(Hero hero) {
        super.applyToHero(hero);
        Player player = hero.getPlayer();
        PotionEffect invishow = new PotionEffect(PotionEffectType.SLOW_DIGGING, 900, 0, false, true);
        player.addPotionEffect(invishow);

        //hide the player
        if (hero.hasParty()) {
            for (Player aplay : player.getServer().getOnlinePlayers()) {
                if ((!hero.getParty().isPartyMember(aplay)) && (!aplay.isOp())) {
                    aplay.hidePlayer(player);
                }
            }

        } else {
            for (Player aplay : player.getServer().getOnlinePlayers()) {
                if (!aplay.isOp()) {
                    aplay.hidePlayer(player);
                }
            }
        }
        for (Entity entity : player.getNearbyEntities(10.0, 10.0, 10.0)) {
            if (!(entity instanceof Creature)) {
                continue;
            }
            Creature creature = (Creature) entity;
            if (creature.getTarget() == null) {
                continue;
            }
            if (!creature.getTarget().equals(player)) {
                continue;
            }
            creature.setTarget((LivingEntity) null);
        }
        //end hide the player
        if (this.applyText != null && this.applyText.length() > 0) {
            Messaging.send(player, this.applyText, player.getDisplayName());
        }
    }

    public void removeFromHero(Hero hero) {
        super.removeFromHero(hero);
        Player player = hero.getPlayer();
        player.removePotionEffect(PotionEffectType.SLOW_DIGGING);
        for (Player onlinePlayer : player.getServer().getOnlinePlayers()) {
            onlinePlayer.showPlayer(player);
        }

        if (this.expireText != null && this.expireText.length() > 0) {
            Messaging.send(player, this.expireText, player.getDisplayName());
        }

    }
}
