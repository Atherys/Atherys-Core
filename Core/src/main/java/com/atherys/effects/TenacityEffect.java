package com.atherys.effects;

import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.Skill;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.entity.Player;

/**
 * Created by pumpapa on 16/02/16.
 */
public class TenacityEffect extends ExpirableEffect {
    private boolean messages;

    public TenacityEffect(Skill skill, long duration, boolean messages,Hero hero) {
        super(skill, "TenacityEffect",hero.getPlayer(), duration);
        types.add(EffectType.BENEFICIAL);
        this.messages = messages;
    }

    @Override
    public void applyToHero(Hero hero) {
        super.applyToHero(hero);
        if (messages) {
            Player player = hero.getPlayer();
            Messaging.send(player, "Your rage makes you unable to be knocked back.");
        }
    }

    @Override
    public void removeFromHero(Hero hero) {
        super.removeFromHero(hero);
        if (messages) {
            Player player = hero.getPlayer();
            Messaging.send(player, "You are once again able to be knocked back.");
        }
    }
}
