package com.atherys.effects;

import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.Skill;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.Set;

/**
 * Created by Arthur on 10/29/2015.
 */
public class VanishEffect extends ExpirableEffect {
    private final Set<String> allowedSkills;

    public VanishEffect(Skill skill, long duration, Set<String> allowedSkills,Hero hero) {
        super(skill, "VanishEff",hero.getPlayer(),duration);
        this.allowedSkills = allowedSkills;
    }

    @Override
    public void applyToHero(Hero hero) {
        super.applyToHero(hero);
        Player player = hero.getPlayer();
        PotionEffect invishow = new PotionEffect(PotionEffectType.SLOW_DIGGING, 900, 0, false, true);
        player.addPotionEffect(invishow);

        //hide the player
        if (hero.hasParty()) {
            for (Player aplay : player.getServer().getOnlinePlayers()) {
                if ((!hero.getParty().isPartyMember(aplay)) && (!aplay.isOp())) {
                    aplay.hidePlayer(player);
                }
            }

        } else {
            for (Player aplay : player.getServer().getOnlinePlayers()) {
                if (!aplay.isOp()) {
                    aplay.hidePlayer(player);
                }
            }
        }
        Messaging.send(player, "You vanished!");
    }

    @Override
    public void removeFromHero(Hero hero) {
        super.removeFromHero(hero);
        Player player = hero.getPlayer();
        player.removePotionEffect(PotionEffectType.SLOW_DIGGING);
        for (Player onlinePlayer : player.getServer().getOnlinePlayers()) {
            onlinePlayer.showPlayer(player);
        }
        Messaging.send(player, "Vanish expired!");
    }

    public Set<String> getAllowedSkills() {
        return allowedSkills;
    }
}
