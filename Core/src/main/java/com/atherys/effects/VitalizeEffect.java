package com.atherys.effects;

import com.herocraftonline.heroes.api.events.HeroRegainHealthEvent;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicHealEffect;
import com.herocraftonline.heroes.characters.skill.Skill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.entity.Player;

public class VitalizeEffect extends PeriodicHealEffect {
    private final String applyText;
    private final String expireText;
    private final double amount;
    private final int manaBonus;
    private final Hero applier;


    public VitalizeEffect(Skill skill, long period, long duration, double amount, Player applier, int manaBonus,Hero hero) {
        this(skill, period, duration, amount, applier, manaBonus,
                SkillConfigManager.getRaw(skill, SkillSetting.APPLY_TEXT, "You feel a bit wiser!"),
                SkillConfigManager.getRaw(skill, SkillSetting.EXPIRE_TEXT, "You no longer feel as wise!")
        );
    }

    public VitalizeEffect(Skill skill, long period, long duration, double amount, Player applier, int manaBonus, String applyText, String expireText) {
        super(skill, "Vitalize",applier, period, duration, amount);
        this.manaBonus = manaBonus;
        this.amount = amount;
        this.applyText = applyText;
        this.expireText = expireText;
        this.applier = plugin.getCharacterManager().getHero(applier);
        this.types.add(EffectType.BENEFICIAL);
        this.types.add(EffectType.HEALING);
    }

    @Override
    public void applyToHero(Hero hero) {
        super.applyToHero(hero);
        Player player = hero.getPlayer();
        Messaging.send(player, this.applyText);
    }

    @Override
    public void tickHero(Hero hero) {
        super.tickHero(hero);
        HeroRegainHealthEvent hrhEvent = new HeroRegainHealthEvent(hero, amount, skill, applier);
        plugin.getServer().getPluginManager().callEvent(hrhEvent);
        if (!hrhEvent.isCancelled()) {
            hero.heal(hrhEvent.getDelta()); // Update to use getDelta in 1.10+
        }
        int newMana = hero.getMana() + manaBonus > hero.getMaxMana() ? hero.getMaxMana() : hero.getMana() + manaBonus;
        hero.setMana(newMana);
    }

    @Override
    public void removeFromHero(Hero hero) {
        super.removeFromHero(hero);
        Player player = hero.getPlayer();
        Messaging.send(player, this.expireText);
    }
}

