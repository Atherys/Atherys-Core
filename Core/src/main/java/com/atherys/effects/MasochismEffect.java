package com.atherys.effects;

import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.Effect;
import com.herocraftonline.heroes.characters.skill.Skill;
import org.bukkit.ChatColor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MasochismEffect extends Effect {
    private Map<Effect, Long> effectlist = new HashMap<>();

    public MasochismEffect(Skill skill) {
        super(skill, "MasochismEffect");
    }

    @Override
    public void applyToHero(Hero hero) {
        super.applyToHero(hero);
        hero.getPlayer().sendMessage(ChatColor.GRAY + "Masochism activated.");
    }

    public void addToList(Effect eff, long effectduration) {
        effectlist.put(eff, effectduration + System.currentTimeMillis());
    }

    public void removeFromList(Effect eff) {
        effectlist.remove(eff);
    }

    public boolean canBeApplied(Effect effect) {
        if (effectlist.containsKey(effect) && effectlist.get(effect).longValue() <= System.currentTimeMillis()) {
            effectlist.remove(effect);
            return false;
        }
        return true;
    }

    public void clearList() {
        effectlist.clear();
    }

    public List<Effect> getEffects() {
        return new ArrayList<>(effectlist.keySet());
    }

    @Override
    public void removeFromHero(Hero hero) {
        super.applyToHero(hero);
        clearList();
        hero.getPlayer().sendMessage(ChatColor.GRAY + "Masochism deactivated.");
    }

}