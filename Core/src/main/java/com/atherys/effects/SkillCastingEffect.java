package com.atherys.effects;

import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.skill.Skill;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pumpapa on 14/10/15.
 */
public class SkillCastingEffect extends PeriodicExpirableEffect {
    private List<double[]> coords;
    private String applyText;
    private String expireText;
    private String interruptionText;
    private boolean slow;
    private int periodNumber = 0;

    public SkillCastingEffect(Skill skill, long duration, boolean slow, String applyText, String expireText, String interruptionText,Hero hero) {
        super(skill, "SkillCasting",hero.getPlayer(), 500, duration);
        this.applyText = applyText;
        this.expireText = expireText;
        this.interruptionText = interruptionText;
        this.slow = slow;
        coords = new ArrayList<>();
        double t = 0;
        int r = 1;
        for (int i = 0; i < (getDuration()/getPeriod()); i++) {
            t += Math.PI/(getDuration()/1000);
            double x = r * Math.cos(t);
            double z = r * Math.sin(t);
            coords.add(new double[] { x, z, 0 });
        }
        if (slow) {
            addMobEffect(2, (int)(duration/50), 3, false);
            addMobEffect(4, (int)(duration/50), 254, false);
            this.types.add(EffectType.WEAKNESS);
        }
    }

    public SkillCastingEffect(Skill skill, long duration, boolean slow, boolean text,Hero hero) {
        this(skill, duration, slow, text ? "$1 begins channeling $2!" : "", text ? "$1 used $2!" : "", text ? "$1 was interrupted from channeling $2!" : "",hero);
    }

    @Override
    public void applyToHero(Hero hero) {
        super.applyToHero(hero);
        Player player = hero.getPlayer();
        broadcast(player.getLocation(), applyText, player.getDisplayName(), skill.getName());
    }

    @Override
    public void removeFromHero(Hero hero) {
        super.removeFromHero(hero);
        Player player = hero.getPlayer();
        if(this.isExpired()) {
            for (double[] coord : coords) {
                Location loc = player.getLocation();
                loc.add(coord[0], 1, coord[1]);
                loc.getWorld().spawnParticle(Particle.REDSTONE, loc, 0, 1, 0.0d, Float.MIN_VALUE, Float.MIN_VALUE );
            }
            broadcast(player.getLocation(), expireText, player.getDisplayName(), skill.getName());
        } else {
            broadcast(player.getLocation(), interruptionText, player.getDisplayName(), skill.getName());
        }
    }

    @Override
    public void tickHero(Hero hero) {
        Player player = hero.getPlayer();
        if (coords.size() <= periodNumber) {
            return;
        }
        double[] tempCoord = coords.get(periodNumber);
        tempCoord[2] = 1;
        coords.set(periodNumber, tempCoord);
        periodNumber++;
        for (double[] coord : coords) {
            Location loc = player.getLocation();
            loc.add(coord[0], 1, coord[1]);
            if (coord[2] == 0) {
                loc.getWorld().spawnParticle(Particle.REDSTONE, loc, 0, 1, Float.MIN_VALUE, Float.MIN_VALUE, Float.MIN_VALUE );
            } else {
                loc.getWorld().spawnParticle(Particle.REDSTONE, loc, 0, 1, 0.0d, 0.0d, Float.MIN_VALUE );
            }
        }
        player.getEyeLocation().getWorld().spawnParticle(Particle.CLOUD, player.getEyeLocation().add(0,1,0), 1 );
    }

    @Override
    public void tickMonster(Monster monster) {
        //N/A
    }

    public boolean hasSlow() {
        return slow;
    }
}
