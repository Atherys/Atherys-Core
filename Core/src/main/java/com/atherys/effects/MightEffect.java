package com.atherys.effects;

import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.Skill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class MightEffect extends ExpirableEffect {
    private double damage;
    private Skill skill;
    private String applyText;
    private String expireText;

    public MightEffect(Skill skill, long duration, double damage,Hero hero) {
        this(skill, duration, damage,
                SkillConfigManager.getRaw(skill, SkillSetting.APPLY_TEXT, "Your muscles bulge with power!"),
                SkillConfigManager.getRaw(skill, SkillSetting.EXPIRE_TEXT, "You feel strength leave your body!")
        ,hero);
    }

    public MightEffect(Skill skill, long duration, double damage, String applyText, String expireText,Hero hero) {
        super(skill, "Might",hero.getPlayer(),duration);
        this.damage = damage;
        this.skill = skill;
        this.types.add(EffectType.BENEFICIAL);
        this.types.add(EffectType.PHYSICAL);
        this.applyText = applyText;
        this.expireText = expireText;
        addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, (int) duration / 50, 0, false, true), false);
    }

    public void applyToHero(Hero hero) {
        super.applyToHero(hero);
        Player player = hero.getPlayer();
        Messaging.send(player, this.applyText);
    }

    public void removeFromHero(Hero hero) {
        super.removeFromHero(hero);
        Player player = hero.getPlayer();
        Messaging.send(player, this.expireText);
    }

    public double getDamage() {
        return damage;
    }
}

