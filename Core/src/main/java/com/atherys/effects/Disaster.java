package com.atherys.effects;

import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.skill.Skill;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

public class Disaster extends PeriodicExpirableEffect {
    private final Player caster;
    private double damage;

    public Disaster(Skill skill, String name, long period, long duration, int damage, Player caster) {
        super(skill, name,caster, period, duration);
        this.caster = caster;
        this.damage = damage;
    }

    public double getPeriodicDamage() {
        return damage;
    }

    public void setPeriodicDamage(double damage) {
        this.damage = damage;
    }

    @Override
    public void tickHero(Hero hero) {
        if (Skill.damageCheck((org.bukkit.entity.LivingEntity) hero.getPlayer(), caster) || ((caster.equals(hero.getPlayer())) && (!plugin.getCharacterManager().getHero(caster).hasEffectType(EffectType.INVULNERABILITY)))) {
	        skill.damageEntity(hero.getPlayer(), caster, damage, DamageCause.CUSTOM, false);
        }
    }

    @Override
    public void tickMonster(Monster mnstr) {

    }

}
