package com.atherys.effects;

import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.Skill;
import net.md_5.bungee.api.ChatColor;

/**
 * Created by pumpapa on 03/11/15.
 */
public class SilenceEffect extends ExpirableEffect {
    boolean message;

    public SilenceEffect(Skill skill, long duration, boolean message,Hero hero) {
        super(skill, "QuietSilence",hero.getPlayer(), duration);
        this.message = message;
        this.types.add(EffectType.DISPELLABLE);
        this.types.add(EffectType.HARMFUL);
        this.types.add(EffectType.SILENCE);
    }

    @Override
    public void applyToHero(Hero hero) {
        super.applyToHero(hero);
        if (message) {
            hero.getPlayer().sendMessage(ChatColor.GRAY + "You are silenced!");
        }
    }

    @Override
    public void removeFromHero(Hero hero) {
        super.removeFromHero(hero);
        if (message) {
            hero.getPlayer().sendMessage(ChatColor.GRAY + "You are no longer silenced!");
        }
    }
}