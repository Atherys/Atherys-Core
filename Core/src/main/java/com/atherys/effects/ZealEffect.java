package com.atherys.effects;

import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.Skill;

/**
 * Created by Arthur on 6/1/2015.
 */
public class ZealEffect extends ExpirableEffect {
    private double mult;
    private String ApplyText;
    private String ExpireText;

    public ZealEffect(Skill skill, long duration, double mult, String applyText, String expireText,Hero hero) {
        super(skill, "Zeal",hero.getPlayer(), duration);
        this.mult = mult;
        this.ApplyText = applyText;
        this.ExpireText = expireText;
        types.add(EffectType.BENEFICIAL);
        types.add(EffectType.MAGIC);
    }

    @Override
    public void applyToHero(Hero hero) {
        super.applyToHero(hero);
        hero.getPlayer().sendMessage(ApplyText);
    }

    @Override
    public void removeFromHero(Hero hero) {
        super.removeFromHero(hero);
        hero.getPlayer().sendMessage(ExpireText);
    }

    public double getMultiplier() {
        return mult;
    }
}
