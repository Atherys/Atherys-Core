package com.atherys.effects;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.Effect;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.skill.Skill;

/**
 * Created by AmourNaif on 4/9/2016.
 */
public class OfficerDeployedEffect extends Effect {
    private final Hero officer;
    public OfficerDeployedEffect(Skill skill, Hero officer) {
        super(skill, "OfficerDeployedEffect");
        this.types.add(EffectType.BENEFICIAL);
        this.officer = officer;
    }
    public void applyToHero(Hero hero) {
        super.applyToHero(hero);
    }
    public void removeFromHero(Hero hero) {
        super.removeFromHero(hero);
    }
    public Hero getOfficer(){
        return officer;
    }
}