package com.atherys.heroesaddon.lib;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by NeumimTo on 29.3.14.
 */
@Deprecated
@Retention(RetentionPolicy.RUNTIME)
public @interface Disabled {
    Reason Reason() default Reason.NOT_NEEDED;

    @Deprecated
    enum Reason {
        NOT_NEEDED, BUGGED
    }
}
