/*
 * Copyright (c) 2016. A'therys Ascended
 *
 * This source code is proprietary software and must not be redistributed without A'therys Ascended's approval
 *
 * File Created by Anonymous
 * File Cleaned Up by willies952002
 */
package com.atherys.heroesaddon;

public class HeroesAddon {

    public static final int PARTICLE_RANGE = 20;
    public static final int BIG_PARTICLE_RANGE = 40;

}
