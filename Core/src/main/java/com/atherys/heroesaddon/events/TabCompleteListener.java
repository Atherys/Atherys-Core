/*
 * Copyright (c) 2016. A'therys Ascended
 *
 * This source code is proprietary software and must not be redistributed without A'therys Ascended's approval
 * 
 * File Created by willies952002
 */
package com.atherys.heroesaddon.events;

import com.atherys.heroesaddon.util.Util;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.server.TabCompleteEvent;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class TabCompleteListener implements Listener {

    private HashMap<UUID, Integer> attempts = new HashMap<>();

    @EventHandler
    public void onTabComplete(TabCompleteEvent event) {
        String buffer = event.getBuffer();
        List<String> completions = event.getCompletions();
        CommandSender sender = event.getSender();
        if (sender instanceof Player) {
            if (sender.isOp()) return;
            if (!buffer.startsWith("/ver")) return;
            completions.clear();
            Player player = (Player)sender;
            int numAttempts = attempts.getOrDefault(player.getUniqueId(), 0) + 1;
            switch (numAttempts) {
                case 1: {
                    Util.sendMsg(player, "&cPermission Denied");
                    attempts.put(player.getUniqueId(), numAttempts);
                    break;
                }
                case 2: {
                    Util.sendMsg(player, "&4I'M WARNING YOU!!");
                    attempts.put(player.getUniqueId(), numAttempts);
                    break;
                }
                case 3: {
                    Util.sendMsg(player, "&6Don't Say That I Didn't Warn You...");
                    attempts.put(player.getUniqueId(), numAttempts);
                    break;
                }
                case 4: {
                    player.kickPlayer(Util.color("&4Kicked By Console:\n\n&6Too Many Tab-Completion Attempts\n&6on Restricted Command"));
                    attempts.remove(player.getUniqueId());
                }
            }
            player.playSound(player.getLocation(), Sound.ENTITY_GHAST_SCREAM, 6, 1);
        }
    }

}
