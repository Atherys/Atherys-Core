package com.atherys.heroesaddon.events;

import com.atherys.core.Core;
import com.dsh105.echopet.EchoPetPlugin;
import com.dsh105.echopet.api.PetManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

/**
 * Created with IntelliJ.
 * User: NeumimTo
 * Date: 19.3.14
 * Time: 21:46
 */
public class PetListener implements Listener {
    private final PetManager manager;

    public PetListener() {
        manager = EchoPetPlugin.getManager();
        if ( manager == null ) {
            Bukkit.getLogger().severe("[AtherysCore] Could not find EchoPet Manager.");
        }
    }

    @EventHandler(priority = EventPriority.LOW, ignoreCancelled = true)
    public void onPlayerCommandPreprocess(PlayerCommandPreprocessEvent event) {
        if (event.getMessage().toLowerCase().startsWith("/pet") || event.getMessage().toLowerCase().startsWith("/home")) {
            final Player p = event.getPlayer();
            if (Core.getHero(p).isInCombat()) {
                p.sendMessage(ChatColor.GRAY + "You cannot use this command while in combat");
                event.setCancelled(true);
            }
        }
    }

    @EventHandler(priority = EventPriority.LOW, ignoreCancelled = true)
    public void onDamage(final EntityDamageByEntityEvent event) {
        if ( manager == null ) return;
        if (event.getEntity() instanceof Player && event.getDamager() instanceof Player) {
            final Player d = (Player) event.getDamager();
            final Player p = (Player) event.getEntity();
            manager.removePets(d, false);
            manager.removePets(p, false);
        }
    }
}
