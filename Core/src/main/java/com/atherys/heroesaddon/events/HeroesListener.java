package com.atherys.heroesaddon.events;

import com.atherys.effects.BulwarkEffect;
import com.atherys.effects.DamageReductionEffect;
import com.atherys.effects.MightEffect;
import com.atherys.effects.VanishEffect;
import com.atherys.core.Core;
import com.herocraftonline.heroes.api.events.HeroJoinPartyEvent;
import com.herocraftonline.heroes.api.events.SkillUseEvent;
import com.herocraftonline.heroes.api.events.WeaponDamageEvent;
import com.herocraftonline.heroes.characters.CharacterTemplate;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.Effect;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.util.Messaging;
import com.sk89q.worldguard.bukkit.BukkitUtil;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.flags.DefaultFlag;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPistonExtendEvent;
import org.bukkit.event.block.BlockPistonRetractEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.EntityTargetEvent;
import org.bukkit.event.player.*;
import org.bukkit.event.vehicle.VehicleExitEvent;

import java.util.List;
import java.util.Set;

public class HeroesListener implements Listener {

    /***
     * Piggify Effect
     ***/

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onVehicleExit(VehicleExitEvent event) {
        if (!(event.getExited() instanceof Player)) {
            return;
        }
        if (Core.getHeroes().getCharacterManager().getHero((Player) event.getExited()).hasEffect("Piggify")) {
            event.setCancelled(true);
        }
    }


    /***
     * Tenacity Effect
     ***/

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onKnockback(PlayerVelocityEvent event) {
        Player player = event.getPlayer();
        Hero hero = Core.getHeroes().getCharacterManager().getHero(player);
        if (hero.hasEffect("Tenacity") || hero.hasEffect("Rampage")) {
            event.setCancelled(true);
        }
    }


    /***
     * Disabling Boats in Combat
     ***/

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onPlayerInteractWithBoat(PlayerInteractEntityEvent event) {
        if (!(event.getRightClicked() instanceof Boat)) {
            return;
        }
        if (Core.getHeroes().getCharacterManager().getHero(event.getPlayer()).isInCombat()) {
            Messaging.send(event.getPlayer(), "You can't use that in combat!");
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onPlayerPlaceBoat(PlayerInteractEvent event) {
        if (!event.hasItem() || event.getItem().getType() != Material.BOAT) {
            return;
        }
        if (Core.getHeroes().getCharacterManager().getHero(event.getPlayer()).isInCombat()) {
            Messaging.send(event.getPlayer(), "You can't use that in combat!");
            event.setCancelled(true);
        }
    }


    /***
     * Handling Bow Shooting
     ***/

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onPlayerInteract(EntityShootBowEvent event) {
        if (!(event.getEntity() instanceof Player)) {
            return;
        }
        Hero hero = Core.getHeroes().getCharacterManager().getHero((Player) event.getEntity());
        for (Effect effect : hero.getEffects()) {
            if (effect.isType(EffectType.INVIS)) {
                hero.removeEffect(effect);
            }
        }
    }

    /*
	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onPlayerQuitSkillRemove(PlayerQuitEvent event){
		Player player= event.getPlayer();
		Hero hero = Core.getHeroes().getCharacterManager().getHero(player);
		if (hero.hasEffect("WolfEffect")) {
			hero.removeEffect(hero.getEffect("WolfEffect"));
		}
    }
    */
	@EventHandler(priority = EventPriority.MONITOR)
	public void onPlayerFristJoin(PlayerJoinEvent event){
		List<String> commands;
        final Player player = event.getPlayer();
        if (!player.hasPlayedBefore()) {
            commands = Core.getInstance().getConfig().getStringList("FirstJoinCommands");
            for (String s2 : commands) {
                s2=s2.replace("%player%",player.getName());
                Bukkit.dispatchCommand(Bukkit.getConsoleSender(), s2);
            }
        }
    }

    /*
    Remove this code once a new Heroes version is out.
    Make sure that the new one uses UUIDs in the "heroes" HashMap for CharacterManager.
    Also, don't worry about this listener conflicting with Heroes. As long as Core has a dependency on Heroes,
    it will be loaded after Heroes is loaded and its listeners will automatically be called after Heroes is called.
    The order of the arraylist of listeners to be called is immutable during runtime, so chill.


    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onPlayerQuit(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        CharacterManager cm = Core.getHeroes().getCharacterManager();
        ///cm.refreshHero(player);
        //cm.removeHero(cm.getHero(player));

        for (Hero h : cm.getHeroes()) {
            if (h.getPlayer().getName().equals(player.getName())) {
                try {
	                Field field = Core.getHeroes().getCharacterManager().getClass().getDeclaredField("heroes");
                    field.setAccessible(true);
                    //plugin.getServer().getLogger().log(Level.SEVERE, Core.getHeroes().getCharacterManager().getClass().getDeclaredFields().toString());

                    Object object = field.get(cm);
                    HashMap hashMap = (HashMap) object;
                    Core.getHeroes().getStorageManager().getStorage().saveHero(h,true);
                    hashMap.remove(player.getName());
                    field.setAccessible(false);
                } catch (Exception e) {
                    plugin.getServer().getLogger().log(Level.SEVERE, "Error on PlayerQuit using collections");
                    //e.printStackTrace();
                }
                break;
            }
        }
    }

*/
    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onKneebreakMove(PlayerMoveEvent event) {

        if ( event.getTo() == null || event.getFrom() == null ) return;
        if ( event.getTo().getBlockX() == event.getFrom().getBlockX() &&
                event.getTo().getBlockY() == event.getFrom().getBlockY() &&
                event.getTo().getBlockZ() == event.getFrom().getBlockZ() ) return;
        if ( event.getPlayer() == null ) return;

        Hero hero = Core.getHeroes().getCharacterManager().getHero(event.getPlayer());

        if ( hero == null ) return;

        if (hero.hasEffect("Kneebreak")) {
            Location from = event.getFrom(), to = event.getTo();
            if (from.getY() < to.getY()) { // OBS: care on what you do with this
                event.getPlayer().teleport
                        (new Location(from.getWorld(), from.getX(), from.getY(), from.getZ(), to.getYaw(), to.getPitch()));
                event.setCancelled(true);
            }
            return;
        }
	    if (hero.hasEffect("Condemn")) {
		    Location from = event.getFrom(), to = event.getTo();
		    if (from.getZ() < to.getZ()) { // OBS: care on what you do with this
			    event.getPlayer().teleport
					    (new Location(from.getWorld(), from.getX(), from.getY(), from.getZ(), to.getYaw(), to.getPitch()));
			    event.setCancelled(true);
		    }
		    if (from.getX() < to.getX()) { // OBS: care on what you do with this
			    event.getPlayer().teleport
					    (new Location(from.getWorld(), from.getX(), from.getY(), from.getZ(), to.getYaw(), to.getPitch()));
			    event.setCancelled(true);
		    }
		    return;
	    }
        return;
    }

        /***
		 * Resolve Effect
		 ***/

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onPlayerMove(PlayerMoveEvent event) {
        if ( event.getTo() == null || event.getFrom() == null ) return;
        if ( event.getTo().getBlockX() == event.getFrom().getBlockX() &&
                event.getTo().getBlockY() == event.getFrom().getBlockY() &&
                event.getTo().getBlockZ() == event.getFrom().getBlockZ() ) return;
        if ( event.getPlayer() == null ) return;

        Hero hero = Core.getHeroes().getCharacterManager().getHero(event.getPlayer());

        if ( hero == null ) return;

        if (hero.hasEffectType(EffectType.WEAKNESS) && !hero.hasEffect("Resolve")) {
            if (event.getTo().getY() > event.getFrom().getY()) {
                Location loc = event.getFrom();
                if (loc.getPitch() != event.getTo().getPitch()) {
                    loc.setPitch(event.getTo().getPitch());
                }
                if (loc.getYaw() != event.getTo().getYaw()) {
                    loc.setYaw(event.getTo().getYaw());
                }
                event.setTo(loc);
            }
        }
    }


    /***
     * Cancelling Enderpearls
     ***/

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onPlayerTeleport(PlayerTeleportEvent event) {
        if ((event.getCause() == PlayerTeleportEvent.TeleportCause.ENDER_PEARL)) {
            event.setCancelled(true);
        }
    }


    /***
     * Damage Handling
     ***/

    @EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
    public void onWeaponDamage(WeaponDamageEvent event) {
        if (event.getCause() != EntityDamageEvent.DamageCause.ENTITY_ATTACK || !(event.getDamager() instanceof Hero) || (event.getDamage() == 0)) {
            return;
        }
        CharacterTemplate hero = event.getDamager();
        if (hero.hasEffect("Might")) {
            double bonus = ((MightEffect) hero.getEffect("Might")).getDamage();
            event.setDamage(event.getDamage() + bonus);
        }
        if (hero.hasEffect("DamageReduction")) {
            double reduction = ((DamageReductionEffect) hero.getEffect("DamageReduction")).getDamageReduction();
            event.setDamage(event.getDamage() - reduction);
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onEntityDamage(WeaponDamageEvent event) {
        if (!(event.getDamager() instanceof Hero)) {
            return;
        }
        Hero hero = (Hero) event.getDamager();
        if (hero.hasEffect("Invisible")) {
            hero.removeEffect(hero.getEffect("Invisible"));
        }
        if (hero.hasEffect("Disguise")) {
            hero.removeEffect(hero.getEffect("Disguise"));
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onDamage(EntityDamageEvent event) {
        if (event.getDamage() > 0 && event.getEntity() instanceof Player) {
            Player player = (Player) event.getEntity();
            Hero hero = Core.getHeroes().getCharacterManager().getHero(player);
            if (event.getCause() == EntityDamageEvent.DamageCause.WITHER && hero.hasEffectType(EffectType.WITHER)) {
                event.setCancelled(true);
            } else if (event.getCause() == EntityDamageEvent.DamageCause.SUFFOCATION &&
                    (player.getEyeLocation().getBlock().hasMetadata("HeroesUnbreakableBlock") || player.getLocation().getBlock().hasMetadata("HeroesUnbreakableBlock"))) {
                event.setDamage(0);
            } else if (hero.hasEffect("Bulwark")) {
                BulwarkEffect bulwark = (BulwarkEffect) hero.getEffect("Bulwark");
                event.setDamage(bulwark.damage(event.getDamage(), player));
            } else if (event instanceof EntityDamageByEntityEvent) {
                EntityDamageByEntityEvent edby = (EntityDamageByEntityEvent) event;
                Entity damager = edby.getDamager();
                if ((damager instanceof Projectile)) {
                    if (!(((Projectile) damager).getShooter() == null)) {
                        damager = (Entity) ((Projectile) damager).getShooter();
                    } else return;
                }
                if (!(damager instanceof Player)) return;
                Hero tHero = Core.getHero((Player) damager);
                if (tHero.hasEffect("Invuln")) event.setCancelled(true);
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onEntityDamageByEntity(EntityDamageByEntityEvent event) {
        if (event.getDamager() instanceof Player && event.getEntity() instanceof Player &&
                event.getEntity().getWorld().getName().equalsIgnoreCase("PVE")) {
            event.setCancelled(true);
        }
    }


    /***
     * Custom Fire Damage
     ***/

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onFireDamage(EntityDamageEvent event) {
        if (event.getDamage() <= 0 || (event.getCause() != EntityDamageEvent.DamageCause.FIRE_TICK) || (!(event.getEntity() instanceof Damageable))) {
            return;
        }
        if ((event.getEntity() instanceof Player) && Core.getHeroes().getCharacterManager().getHero((Player) event.getEntity()).hasEffect("Fireskin")) {
            event.setCancelled(true);
            return;
        }
        double damage = event.getDamage();
        event.setDamage(0);
        event.setCancelled(true);
        ((Damageable) event.getEntity()).damage(damage);
        if (event.getEntity() instanceof LivingEntity) {
            ((LivingEntity) event.getEntity()).setNoDamageTicks(0);
        }
    }


    /***
     * Unbreakable Blocks
     ***/

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onBlockBreak(BlockBreakEvent event) {
        Block block = event.getBlock();
        if (block.hasMetadata("HeroesUnbreakableBlock")) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onBlockPistonExtend(BlockPistonExtendEvent event) {
        for (Block block : event.getBlocks()) {
            if (block.hasMetadata("HeroesUnbreakableBlock")) {
                event.setCancelled(true);
                break;
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onBlockPistonRetract(BlockPistonRetractEvent event) {
        for (Block block : event.getBlocks()) {
            if (block.hasMetadata("HeroesUnbreakableBlock")) {
                event.setCancelled(true);
                break;
            }
        }
    }


    /***
     * Vanish
     ***/

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerAttack(EntityDamageEvent event) {
        if (event.isCancelled()) {
            return;
        }
        if (!(event instanceof EntityDamageByEntityEvent)) {
            return;
        }
        EntityDamageByEntityEvent subevent = (EntityDamageByEntityEvent) event;
        if (!(subevent.getDamager() instanceof Player)) {
            return;
        }
        Player player = (Player) subevent.getDamager();
        Hero hero = Core.getHeroes().getCharacterManager().getHero(player);
        if (hero.hasEffect("VanishEff")) {
            event.setDamage(0);
            event.setCancelled(true);
        }
	    if ((hero.getPlayer().getGameMode().equals(GameMode.SPECTATOR)) || ((hero.getPlayer().getGameMode().equals(GameMode.CREATIVE)))){
		    event.setDamage(0);
		    event.setCancelled(true);
	    }
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onPlayerDamage(EntityDamageEvent event) {
        if (event.isCancelled()) {
            return;
        }
        if (!(event instanceof EntityDamageByEntityEvent)) {
            return;
        }
        EntityDamageByEntityEvent subevent = (EntityDamageByEntityEvent) event;
        if (!(subevent.getDamager() instanceof Player) || !(subevent.getEntity() instanceof Player)) {
            return;
        }
        Player player = (Player) subevent.getEntity();
        Hero phero = Core.getHeroes().getCharacterManager().getHero(player);
        if (phero.hasEffect("VanishEff")) {
            event.setCancelled(true);
        }
	    if ((phero.getPlayer().getGameMode().equals(GameMode.SPECTATOR)) || ((phero.getPlayer().getGameMode().equals(GameMode.CREATIVE)))){
		    event.setCancelled(true);
	    }
    }

    @EventHandler(priority = EventPriority.LOW)
    public void onMobTarget(EntityTargetEvent event) {
        if (event.isCancelled()) {
            return;
        }
        if (!(event.getTarget() instanceof Player)) {
            return;
        }
        Hero hero = Core.getHeroes().getCharacterManager().getHero((Player) event.getTarget());
        if (hero.hasEffect("VanishEff")) {
            event.setTarget(null);
            event.setCancelled(true);
        }
	    if ((hero.getPlayer().getGameMode().equals(GameMode.SPECTATOR)) || ((hero.getPlayer().getGameMode().equals(GameMode.CREATIVE)))){
		    event.setTarget(null);
		    event.setCancelled(true);
	    }
    }


    /***
     * Region check
     ***/

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onSpellUse(SkillUseEvent event) {
        Hero hero = event.getHero();
        Player player = event.getPlayer();
        ApplicableRegionSet regions = Core.getWorldGuard().getRegionContainer().get(player.getWorld()).getApplicableRegions(BukkitUtil.toVector(player.getLocation()));
        if (regions != null) {
            for (ProtectedRegion region : regions) {
                if (region == null) {
                    continue;
                }
                Set<String> commands = region.getFlag(DefaultFlag.BLOCKED_CMDS);
                if (commands != null) {
                    for (String command : commands) {
                        if (!player.isOp() && (command.trim().equalsIgnoreCase("/skill") || command.toLowerCase().replace(" +", " ").trim().contains("/skill " + event.getSkill().getName().toLowerCase()))) {
                            Messaging.send(player, "You are not allowed to use that skill in this area!");
                            event.setCancelled(true);
                            return;
                        }
                    }
                }
            }
        }
	    if ((hero.getPlayer().getGameMode().equals(GameMode.SPECTATOR)) || ((hero.getPlayer().getGameMode().equals(GameMode.CREATIVE)))){
			    Messaging.send(player, "You are not allowed to use skills in this gamemode");
			    event.setCancelled(true);
			    return;
	    }
        if (hero.hasEffect("VanishEff")) {
            VanishEffect vanishEffect = (VanishEffect) hero.getEffect("VanishEff");
            if ((!vanishEffect.getAllowedSkills().contains(event.getSkill().getName().toLowerCase()))) {
                event.setCancelled(true);
                Messaging.send(hero.getPlayer(), "You cant use skills in Vanish.");
            }
        }
    }

    @EventHandler
    public void onPartyJoin (HeroJoinPartyEvent event) {

    }
}
