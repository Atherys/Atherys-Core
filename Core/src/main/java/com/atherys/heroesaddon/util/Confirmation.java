/*
 * Copyright (c) 2016. A'therys Ascended
 *
 * This source code is proprietary software and must not be redistributed without A'therys Ascended's approval
 * 
 * File Created by willies952002
 */
package com.atherys.heroesaddon.util;

import com.atherys.core.commands.AtherysCommand;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.function.Consumer;

public class Confirmation {

    private static boolean autoClose;

    private static HashMap<String, ConfirmActions> actions = new HashMap<>();

    public static void textConfirm(Player player, String title, String yesLabel, String noLabel, Consumer<Player> confirmed, Consumer<Player> cancelled) {
        String id = getRandomID();
        actions.put(id, new ConfirmActions(confirmed, cancelled));
        Util.sendMsg(player, "&8&m-----------------------------------------------------");
        Util.sendMsg(player, "  &e" + Util.stripColor(title));
        BaseComponent[] comps = new ComponentBuilder("  [" + yesLabel + "]")
                .color(ChatColor.GREEN)
                .bold(true)
                .event(Util.clickCmd("/confirmation confirm " + id))
                .append(" or ")
                .color(ChatColor.GRAY)
                .append("[" + noLabel + "]")
                .color(ChatColor.RED)
                .bold(true)
                .event(Util.clickCmd("/confirmation cancel " + id))
                .create();
        player.spigot().sendMessage(comps);
        Util.sendMsg(player, "&8&m-----------------------------------------------------");
    }

    @Deprecated
    public static void simpleConfirm(Player player, String title, Consumer<Player> confirm, String... notes) {
        confirm(player, title, "&aAccept", "&cDeny", confirm, notes);
    }

    @Deprecated
    public static void confirm(Player player, String title, Consumer<Player> confirmed, String... notes) {
        confirm(player, title, confirmed, HumanEntity::closeInventory, notes);
    }

    @Deprecated
    public static void confirm(Player player, String title, Consumer<Player> confirmed, Consumer<Player> cancelled, String... notes) {
        confirm(player, title, "Yes", "No", confirmed, cancelled, notes);
    }

    @Deprecated
    public static void confirm(Player player, String title, String yesLabel, String noLabel, Consumer<Player> confirmed, String... notes) {
        confirm(player, title, yesLabel, noLabel, confirmed, HumanEntity::closeInventory, notes);
    }

    @Deprecated
    public static void confirm(Player player, String title, String yesLabel, String noLabel, Consumer<Player> confirmed, Consumer<Player> cancelled, String... notes) {
        String id = getRandomID();
        Inventory inv = Bukkit.createInventory(null, 9, Util.stripColor(title));
        for (int slot = 0; slot < 4; slot++) inv.setItem(slot, button(yesLabel, 13));
        inv.setItem(4, NMSUtil.Item.setPlayer(buildNote(title, notes, id), player));
        for (int slot = 5; slot < 9; slot++) inv.setItem(slot, button(noLabel, 14));
        actions.put(id, new ConfirmActions(confirmed, cancelled));
        player.openInventory(inv);
    }

    private static ItemStack button(String name, int dur) {
        ItemStack stack = new ItemStack(Material.STAINED_GLASS_PANE);
        stack.setDurability((short)dur);
        ItemMeta meta = stack.getItemMeta();
        meta.setDisplayName(Util.color(name));
        stack.setItemMeta(meta);
        return stack;
    }

    private static ItemStack buildNote(String title, String[] notes, String id) {
        ItemStack stack = new ItemStack(Material.PAPER);
        ItemMeta meta = stack.getItemMeta();
        meta.setDisplayName(Util.color(title));
        if (notes != null) {
            List<String> lore = new ArrayList<>(notes.length);
            Arrays.asList(notes).forEach(line -> {
                if (line != null) lore.add(Util.color(line));
            });
            meta.setLore(lore);
        }
        stack.setItemMeta(meta);
        return NMSUtil.Item.setString(stack, "gui_id", id);
    }

    public static void setAutoClose(boolean state) {
        autoClose = state;
    }

    public static String getRandomID() {
        String id = UUID.randomUUID().toString();
        id = new String(Base64.getEncoder().encode(id.getBytes(StandardCharsets.UTF_8)));
        return id.substring(0, 16).toLowerCase();
    }

    public static class ConfirmationListener implements Listener {

        private boolean buttonClicked = false;

        @EventHandler
        public void onInvClose(InventoryCloseEvent event) {
            if (!validInv(event.getInventory())) return;
            String id = NMSUtil.Item.getString(event.getInventory().getItem(4), "gui_id");
            Player player = NMSUtil.Item.getPlayer(event.getInventory().getItem(4));
            Consumer<Player> cancelAction = actions.get(id).cancelAction;
            if (!buttonClicked && cancelAction != null) cancelAction.accept(player);
            buttonClicked = false;
            event.getHandlers().unregister(this);
        }

        @EventHandler
        public void onInvClick(InventoryClickEvent event) {
            if (!validInv(event.getInventory())) return;
            event.setCancelled(true);
            String id = NMSUtil.Item.getString(event.getInventory().getItem(4), "gui_id");
            Player player = NMSUtil.Item.getPlayer(event.getInventory().getItem(4));
            Consumer<Player> confirmAction = actions.get(id).confirmAction;
            Consumer<Player> cancelAction = actions.get(id).cancelAction;
            int slot = event.getSlot();
            if (0 <= slot && slot < 4) {
                player.playSound(player.getLocation(), Sound.UI_BUTTON_CLICK, 1F, 1F);
                confirmAction.accept(player);
                buttonClicked = true;
                if (autoClose) player.closeInventory();
            }
            if (slot == 4) {
                try {
                    Util.sendMsg(player, "&7" + NMSUtil.Item.getTagJSON(event.getInventory().getItem(4)));
                } catch (Exception e) {
                    Util.printException("Unable to Debug NBT Data", e);
                    Util.sendMsg(player, "&cError: Unable to Debug NBT Data, Please Check Console");
                }
            }
            if (4 < slot && slot < 9) {
                player.playSound(player.getLocation(), Sound.UI_BUTTON_CLICK, 1F, 1F);
                if (cancelAction != null) cancelAction.accept(player);
                buttonClicked = true;
                if (autoClose) player.closeInventory();
            }
        }

        private boolean validInv(Inventory tested) {
            return (NMSUtil.Item.getString(tested.getItem(4), "gui_id") != null)
                    && (NMSUtil.Item.getPlayer(tested.getItem(4)) != null);
        }

    }

    private static class ConfirmActions {

        public Consumer<Player> confirmAction = null;
        public Consumer<Player> cancelAction = null;

        public ConfirmActions(Consumer<Player> confirm, Consumer<Player> cancel) {
            confirmAction = confirm;
            if (cancel != null) cancelAction = cancel;
        }

    }

    public static class ConfirmationCommand extends AtherysCommand {

        public ConfirmationCommand() {
            super("confirmation", 2, 2, true);
        }

        @Override
        public void run(CommandSender sender, String[] args) {
            String id = args[1];
            Util.log("Confirmation ID: " + id);
            Util.log("Available Actions:");
            //actions.forEach((actionId, action) -> Util.log(" - " + actionId + " : " + action));
            ConfirmActions ca = actions.get(id);
            if (ca == null) {
                Util.log("No Confirmation Actions for Given ID");
                return;
            }
            switch (args[0]) {
                case "confirm": {
                    ca.confirmAction.accept((Player)sender);
                    break;
                }
                case "cancel": {
                    ca.cancelAction.accept((Player)sender);
                    break;
                }
            }
        }

    }

}
