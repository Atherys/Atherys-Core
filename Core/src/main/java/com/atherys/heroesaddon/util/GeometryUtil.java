/*
 * Copyright (c) 2016. A'therys Ascended
 *
 * This source code is proprietary software and must not be redistributed without A'therys Ascended's approval
 * 
 * File Created by willies952002
 */
package com.atherys.heroesaddon.util;

import java.util.ArrayList;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.util.Vector;

public class GeometryUtil {

    public static ArrayList<Location> circle(Location center, int particles, double radius) {
        World world = center.getWorld();
        double increment = 6.283185307179586D / particles;
        ArrayList<Location> locations = new ArrayList<>();
        for (int i = 0; i < particles; i++) {
            double angle = i * increment;
            double x = center.getX() + radius * Math.cos(angle);
            double z = center.getZ() + radius * Math.sin(angle);
            locations.add(new Location(world, x, center.getY(), z));
        }
        return locations;
    }

    public static ArrayList<Location> filledCircle(Location center, int basePoings, double radius) {
        ArrayList<Location> locations = new ArrayList<>();
        for (double r = 0.0D; r < radius; r += 1.0D) {
            ArrayList<Location> circ = circle(center, basePoings, r);
            locations.addAll(circ);
        }
        return locations;
    }

    public static ArrayList<Location> blockSquare(Location center, int width) {
        int startX = center.getBlockX() - width;
        int endX = center.getBlockX() + width;
        int startZ = center.getBlockZ() - width;
        int endZ = center.getBlockZ() + width;
        ArrayList<Location> locations = new ArrayList<>();
        int x = startX;
        int y = center.getBlockY();
        for (int z = startZ; z <= endZ; z++) {
            Location l = new Location(center.getWorld(), x, y, z);
            if (!locations.contains(l)) locations.add(l);
        }
        x = endX;
        for (int z = startZ; z <= endZ; z++) {
            Location l = new Location(center.getWorld(), x, y, z);
            if (!locations.contains(l)) locations.add(l);
        }
        int z = startZ;
        for (x = startX; x <= endX; x++) {
            Location l = new Location(center.getWorld(), x, y, z);
            if (!locations.contains(l)) locations.add(l);
        }
        z = endZ;
        for (x = startX; x <= endX; x++) {
            Location l = new Location(center.getWorld(), x, y, z);
            if (!locations.contains(l)) locations.add(l);
        }
        return locations;
    }

    public static ArrayList<Location> line(Location start, Location end, double points) {
        ArrayList<Location> pointList = new ArrayList<>();
        Vector dir = end.toVector()
                .subtract(start.toVector())
                .divide(new Vector(points, points, points));
        double dist = start.distance(end);
        Location to = start.clone();
        double traveled = 0.0D;
        while (traveled < Math.abs(dist)) {
            to.add(dir);
            pointList.add(to);
            traveled = Math.abs(to.distance(start));
        }
        return pointList;
    }

}
