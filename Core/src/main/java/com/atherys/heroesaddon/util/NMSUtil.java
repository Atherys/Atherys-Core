/*
 * Copyright (c) 2016. A'therys Ascended
 *
 * This source code is proprietary software and must not be redistributed without A'therys Ascended's approval
 * 
 * File Created by willies952002
 */
package com.atherys.heroesaddon.util;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class NMSUtil {

    static {
        NMSUtil.Item.init();
    }

    public static class Item {

        private static Method asNMSCopy;
        private static Method asCraftCopy;
        private static Method getTag;
        private static Method setTag;
        private static Method setString;
        private static Method getString;

        public static void init() {
            try {
                asNMSCopy = ReflectionUtils.getMethod(
                        "CraftItemStack",
                        ReflectionUtils.PackageType.CRAFTBUKKIT_INVENTORY,
                        "asNMSCopy",
                        ItemStack.class
                );
                asCraftCopy = ReflectionUtils.getMethod(
                        "CraftItemStack",
                        ReflectionUtils.PackageType.CRAFTBUKKIT_INVENTORY,
                        "asCraftCopy",
                        asNMSCopy.getReturnType()
                );
                getTag = asNMSCopy.getReturnType().getMethod("getTag");
                setTag = asNMSCopy.getReturnType().getMethod("setTag");
                setString = ReflectionUtils.PackageType.MINECRAFT_SERVER
                        .getClass("NBTTagCompound").getMethod("setString", String.class, String.class);
                getString = ReflectionUtils.PackageType.MINECRAFT_SERVER
                        .getClass("NBTTagCompound").getMethod("getString", String.class);
            } catch (Exception e) {
                Util.printException("Failed to Setup Reflection", e);
            }
        }

        public static ItemStack setString(ItemStack stack, String key, String val) {
            try {
                Object nmsStack = asNMSCopy.invoke(null, stack); // CraftItemStack.asNMSCopy(stack);
                Object tag = getTag.invoke(nmsStack); // nmsStack.getTag();
                if (tag != null) {
                    tag = setString.invoke(tag, key, val); // tag.setString("gui_id", id);
                    nmsStack = setTag.invoke(nmsStack, tag); // nmsStack.setTag(tag);
                    return (ItemStack) asCraftCopy.invoke(null, nmsStack); // CraftItemStack.asCraftCopy(nmsStack);
                }
            } catch (Exception e) {
                Util.printException("Unable to Set String to Item NBT", e);
            }
            return null;
        }

        public static String getString(ItemStack stack, String key) {
            try {
                Object nmsStack = asNMSCopy.invoke(null, stack); // CraftItemStack.asNMSCopy(stack);
                Object tag = getTag.invoke(nmsStack); // nmsStack.getTag();
                if (tag != null) return (String) getString.invoke(tag, key); // tag.getString(key);
            } catch (Exception e) {
                Util.printException("Unable to Get String from Item NBT", e);
            }
            return null;
        }

        public static ItemStack setPlayer(ItemStack item, Player player) {
            return setString(item, "player_uuid", player.getUniqueId().toString());
        }

        public static Player getPlayer(ItemStack item) {
            return Bukkit.getPlayer(getString(item, "player_uuid"));
        }

        public static String getTagJSON(ItemStack stack) throws Exception {
            return getTag.invoke(asNMSCopy.invoke(null, stack)).toString();
        }

    }

}
