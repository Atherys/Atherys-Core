package com.atherys.heroesaddon.util;

import com.google.common.collect.HashBiMap;
import org.bukkit.Material;

import java.util.Arrays;

public abstract class IDAPI {

    private static HashBiMap<Integer, Material> materials = IDAPI.setupMap();

    public static Material getMaterialById(int id) {
        return materials.get(id);
    }

    public static int getIdOfMaterial(Material pMat) {
        return materials.inverse().get(pMat);
    }

    public static HashBiMap<Integer, Material> setupMap() {
        HashBiMap<Integer, Material> idmap = HashBiMap.create();
        Arrays.asList(Material.values()).forEach(mat -> idmap.put(mat.getId(), mat));
        return idmap;
    }

}
