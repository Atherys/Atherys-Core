/*
 * Copyright (c) 2016. A'therys Ascended
 *
 * This source code is proprietary software and must not be redistributed without A'therys Ascended's approval
 * 
 * File Created by willies952002
 */
package com.atherys.heroesaddon.util;

public class MathUtil {

    private static final float[] vals = new float[65536];

    static {
        for(int i = 0; i < 65536; ++i) {
            vals[i] = (float) java.lang.Math.sin(i * 3.141592653589793D * 2.0D / 65536.0D);
        }
    }

    public static float sin(float var0) {
        return vals[(int)(var0 * 10430.378F) & '\uffff'];
    }

    public static float cos(float var0) {
        return vals[(int)(var0 * 10430.378F + 16384.0F) & '\uffff'];
    }

}
