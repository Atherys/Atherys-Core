package com.atherys.heroesaddon.util;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.wrappers.EnumWrappers;
import com.herocraftonline.heroes.characters.Hero;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Util {

    public static String color(String line) {
        return ChatColor.translateAlternateColorCodes('&', line);
    }

    public static void sendActionBar(Player player, String msg) {
        player.spigot().sendMessage(
                ChatMessageType.ACTION_BAR,
                string2BaseComponent(color(msg))
        );
    }

    private static BaseComponent[] string2BaseComponent(String line) {
        return new ComponentBuilder(line).create();
    }

    public static void sendMsg(CommandSender sender, String msg) {
        sender.sendMessage(color(msg));
    }

    public static boolean permCheck(CommandSender sender, String perm) {
        return (sender.hasPermission(perm)) || (sender.isOp());
    }

    public static void sendFMsg(CommandSender sender, String format, String... args) {
        sendMsg(sender, String.format(format, args));
    }

    public static void log(String msg) {
        Bukkit.getLogger().log(Level.INFO, msg);
    }

    public static void forEachPlayer(Consumer<Player> action) {
        Bukkit.getOnlinePlayers().forEach(action);
    }

    public static void title(Player player, String title, String subtitle) {

        //Spigot
        ProtocolManager protMan = ProtocolLibrary.getProtocolManager();
        PacketContainer packet = protMan.createPacket(PacketType.Play.Server.TITLE);
        packet.getTitleActions().write(0, EnumWrappers.TitleAction.TIMES);
        packet.getIntegers().write(0, 0).write(1, 60).write(2, 0);
        try {
            protMan.sendServerPacket(player, packet);
        } catch (InvocationTargetException e) {
            printException("Unable to Send Packet", e);
        }
        player.sendTitle(color(title), color(subtitle));

        //player.sendTitle(Title.builder().title(title).subtitle(subtitle).fadeIn(0).stay(60).fadeOut(0).build());
    }

    public static Logger getLogger() {
        return Bukkit.getLogger();
    }

    public static void printException(String message, Throwable exception) {
        getLogger().severe(message);
        String[] ex = ExceptionUtils.getFullStackTrace(exception).split("\n");
        for (String exl : ex) getLogger().severe(exl);
    }

    public static String stripColor(String line) {
        return ChatColor.stripColor(color(line));
    }

    public static void switchLamp(Block block, boolean lighting) throws Exception {
        Object w = ReflectionUtils.getMethod(
                "CraftWorld",
                ReflectionUtils.PackageType.CRAFTBUKKIT,
                "getHandle"
        ).invoke(block.getWorld());
        if (lighting) {
            setWorldStatic(w, true);
            block.setType(Material.REDSTONE_LAMP_ON);
            setWorldStatic(w, false);
        } else {
            block.setType(Material.REDSTONE_LAMP_OFF);
        }
    }

    private static void setWorldStatic(Object obj, boolean static_boolean) throws Exception {
        Class worldClass = ReflectionUtils.PackageType.MINECRAFT_SERVER.getClass("World");
        Field static_field = worldClass.getDeclaredField("isClientSide");
        static_field.setAccessible(true);
        static_field.set(obj, static_boolean);
    }

    public static boolean isOnCooldown(Hero hero, String skill, long cooldown) {
        long time = System.currentTimeMillis();
        return !(hero.getCooldown("Summon") == null || (hero.getCooldown("Summon") - time) < cooldown);
    }

    public static ClickEvent clickCmd(String cmd) {
        return new ClickEvent(ClickEvent.Action.RUN_COMMAND, cmd);
    }

    public static HoverEvent hoverText(String text) {
        return new HoverEvent(HoverEvent.Action.SHOW_TEXT, string2BaseComponent(color(text)));
    }

}
